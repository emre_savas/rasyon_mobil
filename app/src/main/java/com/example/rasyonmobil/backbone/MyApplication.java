package com.example.rasyonmobil.backbone;

import android.app.Application;
import android.content.Context;

import com.example.rasyonmobil.engines.SharedPrefencesEngine;
import com.example.rasyonmobil.util.Util;

import java.text.SimpleDateFormat;

public class MyApplication extends Application {
    public static final String TOKEN_USER_NAME = "hicamp";
    public static final String TOKEN_PASSWORD = "H1C4Mp**";
    public static final String GRANT_TYPE = "password";
    private static SimpleDateFormat dateFormat;
    private static Context context;

    public static SimpleDateFormat getDateFormat() {
        if (dateFormat == null) {
            dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        }
        return dateFormat;
    }

    public static String getMealDeliveryUri() {
        if (Util.isWifiConnected(context)) {
            if (Util.getSSIDofNetwork(context).equals(sPref().getStringPreferences(SharedPrefencesEngine.WIFI_NAME))) {
                return "http://" + sPref().getStringPreferences(SharedPrefencesEngine.IN_IP) + IsTestApi();
            } else {
                return "http://" + sPref().getStringPreferences(SharedPrefencesEngine.OUT_IP) + IsTestApi();
            }
        } else {
            return "http://" + sPref().getStringPreferences(SharedPrefencesEngine.OUT_IP) + IsTestApi();
        }
    }

    public static String IsTestApi() {
        if (sPref().getStringPreferences(SharedPrefencesEngine.TESIS_ADI).contains("test")
                || sPref().getStringPreferences(SharedPrefencesEngine.TESIS_ADI).contains("Test")
                || sPref().getStringPreferences(SharedPrefencesEngine.TESIS_ADI).contains("TEST")) {
            return "/Test.MealDelivery.Api/";
        } else {
            return "/MealDelivery.Api/";
        }
    }

    private static SharedPrefencesEngine sPref() {
        return SharedPrefencesEngine.sharedInstance(context);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //ManagerInitializer.i.init(getApplicationContext());
        context = getApplicationContext();
        //Fabric.with(this, new Crashlytics(), new Answers());
        //Crashlytics.start(this);
    }
}
