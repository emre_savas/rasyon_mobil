package com.example.rasyonmobil.backbone;

import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.rasyonmobil.engines.SharedPrefencesEngine;

import java.lang.reflect.Method;

public class MyBaseActivity extends AppCompatActivity {
    MaterialDialog errorDialog;


    public SharedPrefencesEngine spref() {
        return SharedPrefencesEngine.sharedInstance(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Util.isTablet(this)) {
//            if (getResources().getDisplayMetrics().widthPixels < 601) {
//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//            } else {
//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//            }
//        } else {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        if (getIntent().getExtras() != null && getIntent().getExtras().getSerializable("reception") != null) {
//            receptionInfo = (ReceptionInfo) getIntent().getExtras().getSerializable("reception");
//        }
        if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    protected boolean checkStatOfOp(String stat) {
        if (stat.equalsIgnoreCase("Başlanmadı") || stat.equalsIgnoreCase("İstekte") ||
                stat.equalsIgnoreCase("istekte")) {
            return false;
        } else {
            Toast.makeText(this, stat + " durumundaki işlemleri silemezsiniz.", Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    protected boolean isNurse() {
        return spref().getIntPreferences(SharedPrefencesEngine.USER_LOGIN_TYPE) == 1;
    }

    protected boolean isDoctor() {
        return spref().getIntPreferences(SharedPrefencesEngine.USER_LOGIN_TYPE) == 2;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("Opening Screen", this.getLocalClassName());
    }

    public int getCurrentUserId() {
        return spref().getIntPreferences(SharedPrefencesEngine.USER_ID);
    }
    public String getCurrentUserPass(){
        return spref().getStringPreferences(SharedPrefencesEngine.USER_LOGIN_PASSWORD);
    }

    protected void buildErrorDialog(String message) {
        errorDialog = new MaterialDialog.Builder(this)
                .title("Hata")
                .content(message.replace("&","\n"))
                .buttonsGravity(GravityEnum.END)
                .neutralText("TAMAM").build();
        errorDialog.show();
    }

    protected void buildStandartErrorDialog(String message) {
        errorDialog = new MaterialDialog.Builder(this)
                .title("Hata")
                .content(message)
                .buttonsGravity(GravityEnum.END)
                .neutralText("TAMAM").build();
        errorDialog.show();
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (errorDialog != null && errorDialog.isShowing()) {
            errorDialog.dismiss();
        }
    }

    public boolean ismIsRunning() {
        return mIsRunning;
    }

    protected boolean mIsRunning;
    @Override
    protected void onResume() {
        super.onResume();
        mIsRunning = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mIsRunning=false;
    }

}
