package com.example.rasyonmobil.backbone;
import com.example.rasyonmobil.BuildConfig;

public class MyBuild {
    public static boolean isOnDevelopment() {
        return BuildConfig.BUILD_TYPE.equals("development");
    }
}
