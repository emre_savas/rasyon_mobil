package com.example.rasyonmobil.engines;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.rasyonmobil.model.User;

public class SharedPrefencesEngine {
    public static final String USER_LOGIN_TYPE = "logintype";
    public static final String IS_LOGGED_IN = "isloggedin";
    public static final String USER_ID = "userid";
    public static final String PID="pid";
    public static final String TOKEN = "token";
    public static final String TOKEN_EXPIRES = "tokenexpires";
    public static final String USER_NAME = "username";
    public static final String USER_TITLE = "usertitle";
    public static final String TESIS_ADI = "facilityname";
    public static final String IN_IP = "icip";
    public static final String OUT_IP = "disip";
    public static final String EMERGENCY_IP = "emergencyip";
    public static final String WIFI_NAME = "wifiname";
    public static final String REGION_ID = "regionid";
    public static final String REGION_NAME = "regionname";
    public static final String POOL_ID = "poolid";
    public static final String COUNTER_ID = "counterid";

    public static final String USER_LOGIN_PASSWORD = "userloginpassword";
    public static final String USER_LOGIN_USERNAME = "userloginusername";

    public static SharedPrefencesEngine sharedInstance = null;

    public static SharedPrefencesEngine sharedInstance(Activity context) {

        sharedInstance = new SharedPrefencesEngine(context);
        return sharedInstance;
    }

    public static SharedPrefencesEngine sharedInstance(Context context) {

        sharedInstance = new SharedPrefencesEngine(context);
        return sharedInstance;
    }

    public static void clearSharedInstance() {
        sharedInstance = null;
    }

    private SharedPreferences preferences;

    public SharedPrefencesEngine(Activity context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public SharedPrefencesEngine(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void savePreferences(String key, boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void savePreferences(String key, String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void savePreferences(String key, int value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }





    public String getStringPreferences(String key) {
        return preferences.getString(key, "empty");
    }

    public boolean getbooleanPreferences(String key) {
        return preferences.getBoolean(key, false);
    }

    public Integer getIntPreferences(String key) {
        return preferences.getInt(key, 0);
    }


    public void deleteAllSharedPreferences() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(USER_LOGIN_TYPE);
        editor.remove(PID);
        editor.remove(USER_NAME);
        editor.remove(IS_LOGGED_IN);
        editor.remove(USER_ID);
        editor.remove(TOKEN);
        editor.remove(TOKEN_EXPIRES);
        editor.remove(USER_LOGIN_PASSWORD);
        editor.commit();
    }
    public void deleteUserName(){
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(USER_NAME);
        editor.commit();
    }public void deleteLogin(){
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(USER_NAME);
        editor.remove(IS_LOGGED_IN);
        editor.commit();
    }
}
