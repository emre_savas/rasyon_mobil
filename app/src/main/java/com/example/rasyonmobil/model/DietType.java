package com.example.rasyonmobil.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DietType {
    public DietType(){

    }
    @SerializedName("Id") @Expose public Integer Id;
    @SerializedName("ObjectID") @Expose  public int ObjectID;
    @SerializedName("Name") @Expose  public String Name;
    @SerializedName("Definition") @Expose  public String Definition;
    @SerializedName("AraOgun") @Expose  public Boolean AraOgun;
    @SerializedName("DietType") @Expose  public String DietType;
    @SerializedName("NutritionType ") @Expose  public int NutritionType ;
}
