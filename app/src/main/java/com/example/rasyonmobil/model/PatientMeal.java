package com.example.rasyonmobil.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class PatientMeal {
    public PatientMeal(){

    }
    @SerializedName("ID") @Expose public int ID;
    @SerializedName("PatientDietID") @Expose public int PatientDietID;
    @SerializedName("PatientCompDietID") @Expose public int PatientCompDietID;
    @SerializedName("DietID") @Expose public int DietID;
    @SerializedName("DietTypeID") @Expose public int DietTypeID;
    @SerializedName("MealTypeID") @Expose public int MealTypeID;
    @SerializedName("ClinicID") @Expose public int ClinicID;
    @SerializedName("BedUnitID") @Expose public int BedUnitID;
    @SerializedName("BedID") @Expose public int BedID;
    @SerializedName("ReceptionID") @Expose public int ReceptionID;
    @SerializedName("PatientID") @Expose public int PatientID;
    @SerializedName("ClinicName") @Expose public String ClinicName;
    @SerializedName("BedUnitName") @Expose public String BedUnitName;
    @SerializedName("RoomName") @Expose public String RoomName;
    @SerializedName("BedName") @Expose public String BedName;
    @SerializedName("PatientFullName") @Expose public String PatientFullName;
    @SerializedName("ReceptionNo") @Expose public String ReceptionNo;
    @SerializedName("MealTypeName") @Expose public String MealTypeName;
    @SerializedName("DietType") @Expose public String DietType;
    @SerializedName("PatientAge") @Expose public int PatientAge;
    @SerializedName("PatientGender") @Expose public String PatientGender;
    @SerializedName("MealDate") @Expose public String MealDate;
    @SerializedName("MealType") @Expose public String MealType;
    @SerializedName("Comment") @Expose public String Comment;
    @SerializedName("PatientHasMeal") @Expose public Boolean PatientHasMeal;
    @SerializedName("CompanierHasMeal") @Expose public Boolean CompanierHasMeal;
    @SerializedName("PatientNutrition") @Expose public String PatientNutrition;
    @SerializedName("DeliveryStatus") @Expose public String DeliveryStatus;
    @SerializedName("IsNewDiet") @Expose public Boolean IsNewDiet;
    @SerializedName("PatientDietOwnerOID") @Expose public int PatientDietOwnerOID;
    @SerializedName("RefDietID") @Expose public int RefDietID;
    @SerializedName("RefDietType") @Expose public String RefDietType;
    @SerializedName("RefDietName") @Expose public String RefDietName;
    @SerializedName("CompanierName") @Expose public String CompanierName;

    @SerializedName("PatientEating") @Expose public Boolean PatientEating;
    @SerializedName("CompanierEating") @Expose public Boolean CompanierEating;
    @SerializedName("UserID") @Expose public Integer UserID;
    @SerializedName("CreateDate") @Expose public String CreateDate;
    @SerializedName("PostedStatus") @Expose public Integer PostedStatus;

    @SerializedName("NewDietTypeID") @Expose public Integer NewDietTypeID;
    @SerializedName("ErrorMessage") @Expose public String ErrorMessage;
    @SerializedName("PostAppVers") @Expose public String PostAppVers;
    @SerializedName("CompObjectID") @Expose public String CompObjectID;
}
