package com.example.rasyonmobil.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenObject {
    @SerializedName("access_token") @Expose public String access_token;
    @SerializedName("token_type")   @Expose public String token_type;
    @SerializedName("expires_in")   @Expose public long expires_in;
}
