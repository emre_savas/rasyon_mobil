package com.example.rasyonmobil.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    public User(String userName,String password){
        this.UserName=userName;
        this.Password=password;
    }
    public User(){

    }
    @SerializedName("Id") @Expose public Integer Id;
    @SerializedName("UserID") @Expose  public int UserID;
    @SerializedName("UserName") @Expose  public String UserName;
    @SerializedName("Title") @Expose  public String Title;
    @SerializedName("PID") @Expose  public String PID;
    @SerializedName("PersonnelName") @Expose  public String PersonnelName;
    @SerializedName("PersonnelID") @Expose  public int PersonnelID;
    @SerializedName("Password") @Expose  public String Password;
}
