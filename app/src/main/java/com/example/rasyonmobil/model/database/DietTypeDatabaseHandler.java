package com.example.rasyonmobil.model.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.example.rasyonmobil.model.DietType;
import com.example.rasyonmobil.model.User;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class DietTypeDatabaseHandler {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "PatientFoods";
    private static String TABLE_NAME = "dietType";
    private static final String KEY_ID = "id";
    private static final String KEY_ObjectID = "ObjectID";
    private static final String KEY_Name = "Name";
    private static final String KEY_Definition = "Definition";
    private static final String KEY_AraOgun = "AraOgun";
    private static final String KEY_DietType = "DietType";
    private static final String KEY_NutritionType = "NutritionType";
    private static SQLiteDatabase DATABASE=null;
    public DietTypeDatabaseHandler(Context context) {
        DATABASE=context.openOrCreateDatabase(DATABASE_NAME,MODE_PRIVATE,null);
        //3rd argument to be passed is CursorFactory instance
        onCreate(DATABASE);
    }
    public  void setTableName(String TableName){
        TABLE_NAME=TableName;
    }

    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE Table if not exists "+TABLE_NAME+" (id INTEGER PRIMARY KEY ,"+KEY_ObjectID+" INTEGER,"+KEY_Name+" VARCHAR," +
                ""+KEY_Definition+" VARCHAR,"+KEY_AraOgun+" INTEGER,"+KEY_DietType+" VARCHAR,"+KEY_NutritionType+" INTEGER)";
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade() {
        DATABASE.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(DATABASE);
    }

    public void addDietType(DietType dietType) {

        try {
            String sqlString="INSERT INTO "+TABLE_NAME+" ("+KEY_ObjectID+","+KEY_Name+","+KEY_Definition+","+KEY_AraOgun+","+KEY_DietType+","+KEY_NutritionType+") VALUES (?,?,?,?,?,?)";
            SQLiteStatement sqLiteStatement=DATABASE.compileStatement(sqlString);
            sqLiteStatement.bindDouble(1,dietType.ObjectID);
            sqLiteStatement.bindString(2,(dietType.Name==null || dietType.Name=="")?" ":dietType.Name);
            sqLiteStatement.bindString(3,(dietType.Definition==null || dietType.Definition=="")?" ":dietType.Definition);
            sqLiteStatement.bindDouble(4,dietType.AraOgun ?1:0);
            sqLiteStatement.bindString(5,(dietType.DietType==null || dietType.DietType=="")?" ":dietType.DietType);
            sqLiteStatement.bindDouble(6,dietType.NutritionType);
            sqLiteStatement.execute();
        }catch (Exception e){
            e.printStackTrace();
            Log.i("PatientFoods","tabloya veri güncellemeda hata ! "+e.getMessage());
        }

    }

//    public User getUser(String userName) {
//        User user=new User();
//        try {
//            Cursor cursor = DATABASE.query(TABLE_NAME, new String[] { KEY_ID,
//                            KEY_ObjectID, KEY_Name ,KEY_Definition,KEY_AraOgun,KEY_DietType,KEY_NutritionType}, KEY_USER_NAME + "=?",
//                    new String[] { userName.toLowerCase() }, null, null, null, null);
//            if (cursor != null && cursor.getCount()>0 ){
//                cursor.moveToFirst();
//                int idIx=cursor.getColumnIndex(KEY_ID);
//                int nameIx=cursor.getColumnIndex(KEY_USER_NAME);
//                int paswordIx=cursor.getColumnIndex(KEY_PASSWORD);
//                int userIdIx=cursor.getColumnIndex(KEY_USERID);
//                int titleIx=cursor.getColumnIndex(KEY_TITLE);
//                int pidIx=cursor.getColumnIndex(KEY_PID);
//                int personelNameIx=cursor.getColumnIndex(KEY_PERSONNELNAME);
//                int personelIdIx=cursor.getColumnIndex(KEY_PERSONNELID);
//
//                user.Id=cursor.getInt(idIx);
//                user.UserName=cursor.getString(nameIx)==null?" ":cursor.getString(nameIx);
//                user.Password=cursor.getString(paswordIx)==null?" ":cursor.getString(paswordIx);
//                user.UserID=cursor.getInt((userIdIx));
//                user.Title=cursor.getString(titleIx)==null?" ":cursor.getString(titleIx);
//                user.PID=cursor.getString(pidIx)==null?" ":cursor.getString(pidIx);
//                user.PersonnelName=cursor.getString(personelNameIx)==null?" ":cursor.getString(personelNameIx);
//                user.PersonnelID=cursor.getInt(personelIdIx);
//            }
//        }catch (Exception e){
//
//        }
//        return user;
//    }

    public List<DietType> getAllDietType(Integer araOgunStatus) {
        List<DietType> dietTypeList = new ArrayList<DietType>();
        // Select All Query
        String selectQuery = "";
        Cursor cursor;
        araOgunStatus=0;
        if (araOgunStatus==1){//normal
            selectQuery= "SELECT  * FROM " + TABLE_NAME +" WHERE "+KEY_AraOgun+"=?";
            cursor = DATABASE.rawQuery(selectQuery, new String[] { String.valueOf(0)});
        }else if (araOgunStatus==2){//ara öğün
            selectQuery="SELECT  * FROM " + TABLE_NAME +" WHERE "+KEY_AraOgun+"=?";
            cursor = DATABASE.rawQuery(selectQuery, new String[] { String.valueOf(1)});
        }else{
            selectQuery = "SELECT  * FROM " + TABLE_NAME ;
            cursor = DATABASE.rawQuery(selectQuery, null);
        }


        // looping through all rows and adding to list
        try {
            if (cursor != null && cursor.getCount()>0) {
                while (cursor.moveToNext()){
                    DietType dietType=new DietType();
                    dietType.Id=cursor.getInt(0);
                    dietType.ObjectID=cursor.getInt(1);
                    dietType.Name=cursor.getString(2)==null?" ":cursor.getString(2);
                    dietType.Definition=cursor.getString(3)==null?" ":cursor.getString(3);
                    dietType.AraOgun=cursor.getInt(4)==0?false:true;
                    dietType.DietType=cursor.getString(5)==null?" ":cursor.getString(5);
                    dietType.NutritionType=cursor.getInt(6);
                    dietTypeList.add(dietType);
                }
            }

        }catch (Exception e){
e.printStackTrace();
        }


        return dietTypeList;
    }

//    public int updateContact(User user) {
//        ContentValues values = new ContentValues();
//        values.put(KEY_USER_NAME, user.UserName);
//        values.put(KEY_PASSWORD, user.Password);
//
//        // updating row
//        return DATABASE.update(TABLE_CONTACTS, values, KEY + " = ?",
//                new String[] { String.valueOf(exampleObject.receptionId) });
//    }

//    public void deleteContact(ExampleObject contact) {
//
//        DATABASE.delete(TABLE_CONTACTS, KEY_ID + " = ?",
//                new String[] { String.valueOf(contact.receptionId) });
//        DATABASE.close();
//    }
//    public void deleteContact(Integer receptionId) {
//        DATABASE.delete(TABLE_CONTACTS, KEY_ID + " = ?",
//                new String[] { String.valueOf(receptionId) });
//        DATABASE.close();
//    }

    public int getDataCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;

        Cursor cursor = DATABASE.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

}