package com.example.rasyonmobil.model.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.example.rasyonmobil.engines.PatientMealPostType;
import com.example.rasyonmobil.model.PatientMeal;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class PatientMealDatabaseHandler {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "PatientFoods";
    private static String TABLE_CONTACTS = "PatientFoods";
    private static final String KEY_ID = "id";
    private static final String KEY_PatientDietID = "PatientDietID";
    private static final String KEY_PatientCompDietID = "PatientCompDietID";
    private static final String KEY_DietID = "DietID";
    private static final String KEY_DietTypeID = "DietTypeID";
    private static final String KEY_MealTypeID = "MealTypeID";
    private static final String KEY_ClinicID = "ClinicID";
    private static final String KEY_BedUnitID = "BedUnitID";
    private static final String KEY_BedID = "BedID";
    private static final String KEY_ReceptionID = "ReceptionID";
    private static final String KEY_PatientID = "PatientID";
    private static final String KEY_ClinicName = "ClinicName";
    private static final String KEY_BedUnitName = "BedUnitName";
    private static final String KEY_RoomName = "RoomName";
    private static final String KEY_BedName = "BedName";
    private static final String KEY_PatientFullName = "PatientFullName";
    private static final String KEY_ReceptionNo = "ReceptionNo";
    private static final String KEY_MealTypeName = "MealTypeName";
    private static final String KEY_DietType = "DietType";
    private static final String KEY_PatientAge = "PatientAge";
    private static final String KEY_PatientGender = "PatientGender";
    private static final String KEY_MealDate = "MealDate";
    private static final String KEY_MealType = "MealType";
    private static final String KEY_Comment = "Comment";
    private static final String KEY_PatientHasMeal = "PatientHasMeal";
    private static final String KEY_CompanierHasMeal = "CompanierHasMeal";
    private static final String KEY_PatientNutrition = "PatientNutrition";
    private static final String KEY_DeliveryStatus = "DeliveryStatus";
    private static final String KEY_IsNewDiet = "IsNewDiet";
    private static final String KEY_PatientDietOwnerOID = "PatientDietOwnerOID";
    private static final String KEY_RefDietID = "RefDietID";
    private static final String KEY_RefDietType = "RefDietType";
    private static final String KEY_RefDietName= "RefDietName";
    private static final String KEY_CompanierName = "CompanierName";
    private static final String KEY_CompObjectID = "CompObjectID";

    private static final String KEY_PatientEating = "PatientEating";
    private static final String KEY_CompanierEating = "CompanierEating";
    private static final String KEY_UserID = "UserID";
    private static final String KEY_CreateDate = "CreateDate";
    private static final String KEY_PostedStatus = "PostedStatus";
    private static final String KEY_NewDietTypeID = "NewDietTypeID";
    private static final String KEY_ErrorMessage = "ErrorMessage";

    private static SQLiteDatabase DATABASE=null;
    public PatientMealDatabaseHandler(Context context) {
        DATABASE=context.openOrCreateDatabase(DATABASE_NAME,MODE_PRIVATE,null);
        //3rd argument to be passed is CursorFactory instance
        onCreate(DATABASE);
    }

     public  void setTableContact(String TableName){
        TABLE_CONTACTS=TableName;
     }

    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE Table if not exists "+TABLE_CONTACTS+" (id INTEGER PRIMARY KEY ,"+KEY_PatientDietID+" INTEGER,"+KEY_PatientCompDietID+" INTEGER" +
                ","+KEY_DietID+" INTEGER,"+KEY_DietTypeID+" INTEGER,"+KEY_MealTypeID+" INTEGER,"+KEY_ClinicID+" INTEGER,"+KEY_BedUnitID+" INTEGER," +
                ""+KEY_BedID+" INTEGER,"+KEY_ReceptionID+" INTEGER,"+KEY_PatientID+" INTEGER,"+KEY_ClinicName+" VARCHAR,"+KEY_BedUnitName+" VARCHAR,"+KEY_RoomName+" VARCHAR,"+KEY_BedName+" VARCHAR,"+KEY_PatientFullName+" VARCHAR,"+KEY_ReceptionNo+" VARCHAR,"+KEY_MealTypeName+" VARCHAR," +
                ""+KEY_DietType+" VARCHAR,"+KEY_PatientAge+" INTEGER,"+KEY_PatientGender+" VARCHAR,"+KEY_MealDate+" VARCHAR," +
                ""+KEY_MealType+" VARCHAR,"+KEY_Comment+" VARCHAR,"+KEY_PatientHasMeal+" INTEGER,"+KEY_CompanierHasMeal+" INTEGER,"+KEY_PatientNutrition+" VARCHAR," +
                ""+KEY_DeliveryStatus+" VARCHAR,"+KEY_IsNewDiet+" INTEGER,"+KEY_PatientDietOwnerOID+" INTEGER,"+KEY_RefDietID+" INTEGER,"+KEY_RefDietType+" VARCHAR," +
                ""+KEY_RefDietName+" VARCHAR,"+KEY_CompanierName+" VARCHAR," +
                ""+KEY_PatientEating+" INTEGER,"+KEY_CompanierEating+" INTEGER,"+KEY_UserID+" INTEGER,"+KEY_CreateDate+" VARCHAR,"+KEY_PostedStatus+" INTEGER,"+KEY_NewDietTypeID+" INTEGER,"+KEY_ErrorMessage+" VARCHAR, "+KEY_CompObjectID+" VARCHAR)";
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade() {
        DATABASE.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        onCreate(DATABASE);
    }

    public Integer addPatientMeal(PatientMeal patientMeal) {
        onCreate(DATABASE);

        try {
            String sqlString="INSERT INTO "+TABLE_CONTACTS+" ("+KEY_PatientDietID+","+KEY_PatientCompDietID +
                    ","+KEY_DietID+","+KEY_DietTypeID+","+KEY_MealTypeID+","+KEY_ClinicID+","+KEY_BedUnitID+"," +
                    ""+KEY_BedID+","+KEY_ReceptionID+" ,"+KEY_PatientID+" ,"+KEY_ClinicName+" ,"+KEY_BedUnitName+" " +
                    ","+KEY_RoomName+","+KEY_BedName+","+KEY_PatientFullName+" ,"+KEY_ReceptionNo+" ,"+KEY_MealTypeName+" ," +
                    ""+KEY_DietType+" ,"+KEY_PatientAge+" ,"+KEY_PatientGender+" ,"+KEY_MealDate+" ," +
                    ""+KEY_MealType+" ,"+KEY_Comment+" ,"+KEY_PatientHasMeal+" ,"+KEY_CompanierHasMeal+" ,"+KEY_PatientNutrition+" ," +
                    ""+KEY_DeliveryStatus+" ,"+KEY_IsNewDiet+" ,"+KEY_PatientDietOwnerOID+" ,"+KEY_RefDietID+" ,"+KEY_RefDietType+" ," +
                    ""+KEY_RefDietName+" ,"+KEY_CompanierName+","+KEY_PatientEating+","+KEY_CompanierEating+","+KEY_UserID+"" +
                    ","+KEY_CreateDate+","+KEY_PostedStatus+","+KEY_NewDietTypeID+","+KEY_ErrorMessage+","+KEY_CompObjectID+") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            SQLiteStatement sqLiteStatement=DATABASE.compileStatement(sqlString);
            sqLiteStatement.bindDouble(1,patientMeal.PatientDietID);
            sqLiteStatement.bindDouble(2,patientMeal.PatientCompDietID);
            sqLiteStatement.bindDouble(3,patientMeal.DietID);
            sqLiteStatement.bindDouble(4,patientMeal.DietTypeID);
            sqLiteStatement.bindDouble(5,patientMeal.MealTypeID);
            sqLiteStatement.bindDouble(6,patientMeal.ClinicID);
            sqLiteStatement.bindDouble(7,patientMeal.BedUnitID);
            sqLiteStatement.bindDouble(8,patientMeal.BedID);
            sqLiteStatement.bindDouble(9,patientMeal.ReceptionID);
            sqLiteStatement.bindDouble(10,patientMeal.PatientID);
            sqLiteStatement.bindString(11,patientMeal.ClinicName ==null ? " ":patientMeal.ClinicName );
            sqLiteStatement.bindString(12,patientMeal.BedUnitName==null ? " ":patientMeal.BedUnitName );
            sqLiteStatement.bindString(13,patientMeal.RoomName==null ? " ":patientMeal.RoomName);
            sqLiteStatement.bindString(14,patientMeal.BedName==null ? " ":patientMeal.BedName);
            sqLiteStatement.bindString(15,patientMeal.PatientFullName==null ? " ":patientMeal.PatientFullName);
            sqLiteStatement.bindString(16,patientMeal.ReceptionNo==null ? " ":patientMeal.ReceptionNo);
            sqLiteStatement.bindString(17,patientMeal.MealTypeName==null ? " ":patientMeal.MealTypeName);
            sqLiteStatement.bindString(18,patientMeal.DietType==null ? " ":patientMeal.DietType);
            sqLiteStatement.bindDouble(19,patientMeal.PatientAge);
            sqLiteStatement.bindString(20,patientMeal.PatientGender==null ? " ":patientMeal.PatientGender);
            sqLiteStatement.bindString(21,patientMeal.MealDate==null ? " ":patientMeal.MealDate);
            sqLiteStatement.bindString(22,patientMeal.MealType==null ? " ":patientMeal.MealType);
            sqLiteStatement.bindString(23,patientMeal.Comment==null ? " ":patientMeal.Comment);
            sqLiteStatement.bindDouble(24,patientMeal.PatientHasMeal?1:0);
            sqLiteStatement.bindDouble(25,patientMeal.CompanierHasMeal?1:0);
            sqLiteStatement.bindString(26,patientMeal.PatientNutrition==null ? " ":patientMeal.PatientNutrition);
            sqLiteStatement.bindString(27,patientMeal.DeliveryStatus==null ? " ":patientMeal.DeliveryStatus);
            sqLiteStatement.bindDouble(28,patientMeal.IsNewDiet?1:0);
            sqLiteStatement.bindDouble(29,patientMeal.PatientDietOwnerOID);
            sqLiteStatement.bindDouble(30,patientMeal.RefDietID);
            sqLiteStatement.bindString(31,patientMeal.RefDietType==null ? " ":patientMeal.RefDietType);
            sqLiteStatement.bindString(32,patientMeal.RefDietName==null ? " ":patientMeal.RefDietName);
            sqLiteStatement.bindString(33,patientMeal.CompanierName==null ? " ":patientMeal.CompanierName);

            sqLiteStatement.bindDouble(34,patientMeal.PatientEating?1:0);
            sqLiteStatement.bindDouble(35,patientMeal.CompanierEating?1:0);
            sqLiteStatement.bindDouble(36,patientMeal.UserID);
            sqLiteStatement.bindString(37,patientMeal.CreateDate==null ? " ":patientMeal.CreateDate);
            sqLiteStatement.bindDouble(38,patientMeal.PostedStatus);
            sqLiteStatement.bindDouble(39,patientMeal.NewDietTypeID);
            sqLiteStatement.bindString(40,patientMeal.ErrorMessage==null ? " ":patientMeal.ErrorMessage);
            sqLiteStatement.bindString(41,patientMeal.CompObjectID);
            sqLiteStatement.execute();
            return 1;
        }catch (Exception e){
            e.printStackTrace();
            Log.i("PatientFoods","tabloya veri güncellemeda hata ! "+e.getMessage());
            return 0;
        }

    }
    public PatientMeal getPatientMealByReceptionNoNotDash(String receptionNo,String mealType) {
        PatientMeal patientMeal=new PatientMeal();
        try {
            Cursor cursor = DATABASE.query(TABLE_CONTACTS, new String[] { KEY_ID,
                            KEY_PatientDietID,KEY_PatientCompDietID,KEY_DietID,KEY_DietTypeID,KEY_MealTypeID,KEY_ClinicID,KEY_BedUnitID,
                            KEY_BedID,KEY_ReceptionID,KEY_PatientID,KEY_ClinicName,KEY_BedUnitName,
                            KEY_RoomName,KEY_BedName,KEY_PatientFullName,KEY_ReceptionNo,KEY_MealTypeName,
                            KEY_DietType,KEY_PatientAge,KEY_PatientGender,KEY_MealDate,
                            KEY_MealType,KEY_Comment,KEY_PatientHasMeal,KEY_CompanierHasMeal,KEY_PatientNutrition,
                            KEY_DeliveryStatus,KEY_IsNewDiet,KEY_PatientDietOwnerOID,KEY_RefDietID,KEY_RefDietType,
                            KEY_RefDietName,KEY_CompanierName,KEY_PatientEating,KEY_CompanierEating,KEY_UserID,KEY_CreateDate,KEY_PostedStatus,KEY_NewDietTypeID,KEY_ErrorMessage,KEY_CompObjectID}, KEY_ReceptionNo + " LIKE ? and " +KEY_MealTypeName + "=?",
                    new String[] { String.valueOf(receptionNo+"%"),String.valueOf(mealType) }, null, null, null, null);
            if (cursor != null && cursor.getCount()>0){
                cursor.moveToFirst();
                patientMeal.ID=cursor.getInt(0);
                patientMeal.PatientDietID=cursor.getInt(1);
                patientMeal.PatientCompDietID=cursor.getInt(2);
                patientMeal.DietID=cursor.getInt(3);
                patientMeal.DietTypeID=cursor.getInt(4);
                patientMeal.MealTypeID=cursor.getInt(5);
                patientMeal.ClinicID=cursor.getInt(6);
                patientMeal.BedUnitID=cursor.getInt(7);
                patientMeal.BedID=cursor.getInt(8);
                patientMeal.ReceptionID=cursor.getInt(9);
                patientMeal.PatientID=cursor.getInt(10);
                patientMeal.ClinicName=cursor.getString(11);
                patientMeal.BedUnitName=cursor.getString(12);
                patientMeal.RoomName=cursor.getString(13);
                patientMeal.BedName=cursor.getString(14);
                patientMeal.PatientFullName=cursor.getString(15);
                patientMeal.ReceptionNo=cursor.getString(16);
                patientMeal.MealTypeName=cursor.getString(17);
                patientMeal.DietType=cursor.getString(18);
                patientMeal.PatientAge=cursor.getInt(19);
                patientMeal.PatientGender=cursor.getString(20);
                patientMeal.MealDate=cursor.getString(21);
                patientMeal.MealType=cursor.getString(22);
                patientMeal.Comment=cursor.getString(23);
                patientMeal.PatientHasMeal=cursor.getInt(24)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.CompanierHasMeal=cursor.getInt(25)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.PatientNutrition=cursor.getString(26);
                patientMeal.DeliveryStatus=cursor.getString(27);
                patientMeal.IsNewDiet=cursor.getInt(28)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.PatientDietOwnerOID=cursor.getInt(29);
                patientMeal.RefDietID=cursor.getInt(30);
                patientMeal.RefDietType=cursor.getString(31);
                patientMeal.RefDietName=cursor.getString(32);
                patientMeal.CompanierName=cursor.getString(33);

                patientMeal.PatientEating=cursor.getInt(34)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.CompanierEating=cursor.getInt(35)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.UserID=cursor.getInt(36);
                patientMeal.CreateDate=cursor.getString(37);
                patientMeal.PostedStatus=cursor.getInt(38);
                patientMeal.NewDietTypeID=cursor.getInt(39);
                patientMeal.ErrorMessage=cursor.getString(40);
                patientMeal.CompObjectID=cursor.getString(41);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return patientMeal;
    }

    public PatientMeal getPatientMealByReceptionNo(String receptionNo,String mealType) {
        PatientMeal patientMeal=new PatientMeal();
        try {
            Cursor cursor = DATABASE.query(TABLE_CONTACTS, new String[] { KEY_ID,
                            KEY_PatientDietID,KEY_PatientCompDietID,KEY_DietID,KEY_DietTypeID,KEY_MealTypeID,KEY_ClinicID,KEY_BedUnitID,
                            KEY_BedID,KEY_ReceptionID,KEY_PatientID,KEY_ClinicName,KEY_BedUnitName,
                            KEY_RoomName,KEY_BedName,KEY_PatientFullName,KEY_ReceptionNo,KEY_MealTypeName,
                            KEY_DietType,KEY_PatientAge,KEY_PatientGender,KEY_MealDate,
                            KEY_MealType,KEY_Comment,KEY_PatientHasMeal,KEY_CompanierHasMeal,KEY_PatientNutrition,
                            KEY_DeliveryStatus,KEY_IsNewDiet,KEY_PatientDietOwnerOID,KEY_RefDietID,KEY_RefDietType,
                            KEY_RefDietName,KEY_CompanierName,KEY_PatientEating,KEY_CompanierEating,KEY_UserID,KEY_CreateDate,KEY_PostedStatus,KEY_NewDietTypeID,KEY_ErrorMessage,KEY_CompObjectID}, KEY_ReceptionNo + "=? and " +KEY_MealTypeName + "=?",
                    new String[] { String.valueOf(receptionNo),String.valueOf(mealType) }, null, null, null, null);
            if (cursor != null && cursor.getCount()>0){
                cursor.moveToFirst();
                patientMeal.ID=cursor.getInt(0);
                patientMeal.PatientDietID=cursor.getInt(1);
                patientMeal.PatientCompDietID=cursor.getInt(2);
                patientMeal.DietID=cursor.getInt(3);
                patientMeal.DietTypeID=cursor.getInt(4);
                patientMeal.MealTypeID=cursor.getInt(5);
                patientMeal.ClinicID=cursor.getInt(6);
                patientMeal.BedUnitID=cursor.getInt(7);
                patientMeal.BedID=cursor.getInt(8);
                patientMeal.ReceptionID=cursor.getInt(9);
                patientMeal.PatientID=cursor.getInt(10);
                patientMeal.ClinicName=cursor.getString(11);
                patientMeal.BedUnitName=cursor.getString(12);
                patientMeal.RoomName=cursor.getString(13);
                patientMeal.BedName=cursor.getString(14);
                patientMeal.PatientFullName=cursor.getString(15);
                patientMeal.ReceptionNo=cursor.getString(16);
                patientMeal.MealTypeName=cursor.getString(17);
                patientMeal.DietType=cursor.getString(18);
                patientMeal.PatientAge=cursor.getInt(19);
                patientMeal.PatientGender=cursor.getString(20);
                patientMeal.MealDate=cursor.getString(21);
                patientMeal.MealType=cursor.getString(22);
                patientMeal.Comment=cursor.getString(23);
                patientMeal.PatientHasMeal=cursor.getInt(24)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.CompanierHasMeal=cursor.getInt(25)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.PatientNutrition=cursor.getString(26);
                patientMeal.DeliveryStatus=cursor.getString(27);
                patientMeal.IsNewDiet=cursor.getInt(28)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.PatientDietOwnerOID=cursor.getInt(29);
                patientMeal.RefDietID=cursor.getInt(30);
                patientMeal.RefDietType=cursor.getString(31);
                patientMeal.RefDietName=cursor.getString(32);
                patientMeal.CompanierName=cursor.getString(33);

                patientMeal.PatientEating=cursor.getInt(34)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.CompanierEating=cursor.getInt(35)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.UserID=cursor.getInt(36);
                patientMeal.CreateDate=cursor.getString(37);
                patientMeal.PostedStatus=cursor.getInt(38);
                patientMeal.NewDietTypeID=cursor.getInt(39);
                patientMeal.ErrorMessage=cursor.getString(40);
                patientMeal.CompObjectID=cursor.getString(41);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return patientMeal;
    }
    public PatientMeal getPatientMeal(String id) {
        PatientMeal patientMeal=new PatientMeal();
        try {
            Cursor cursor = DATABASE.query(TABLE_CONTACTS, new String[] { KEY_ID,
                            KEY_PatientDietID,KEY_PatientCompDietID,KEY_DietID,KEY_DietTypeID,KEY_MealTypeID,KEY_ClinicID,KEY_BedUnitID,
                            KEY_BedID,KEY_ReceptionID,KEY_PatientID,KEY_ClinicName,KEY_BedUnitName,
                            KEY_RoomName,KEY_BedName,KEY_PatientFullName,KEY_ReceptionNo,KEY_MealTypeName,
                            KEY_DietType,KEY_PatientAge,KEY_PatientGender,KEY_MealDate,
                            KEY_MealType,KEY_Comment,KEY_PatientHasMeal,KEY_CompanierHasMeal,KEY_PatientNutrition,
                            KEY_DeliveryStatus,KEY_IsNewDiet,KEY_PatientDietOwnerOID,KEY_RefDietID,KEY_RefDietType,
                            KEY_RefDietName,KEY_CompanierName,KEY_PatientEating,KEY_CompanierEating,KEY_UserID,KEY_CreateDate,KEY_PostedStatus,KEY_NewDietTypeID,KEY_ErrorMessage,KEY_CompObjectID}, KEY_ID + "=?",
                    new String[] { String.valueOf(id) }, null, null, null, null);
            if (cursor != null && cursor.getCount()>0){
                cursor.moveToFirst();
                patientMeal.ID=cursor.getInt(0);
                patientMeal.PatientDietID=cursor.getInt(1);
                patientMeal.PatientCompDietID=cursor.getInt(2);
                patientMeal.DietID=cursor.getInt(3);
                patientMeal.DietTypeID=cursor.getInt(4);
                patientMeal.MealTypeID=cursor.getInt(5);
                patientMeal.ClinicID=cursor.getInt(6);
                patientMeal.BedUnitID=cursor.getInt(7);
                patientMeal.BedID=cursor.getInt(8);
                patientMeal.ReceptionID=cursor.getInt(9);
                patientMeal.PatientID=cursor.getInt(10);
                patientMeal.ClinicName=cursor.getString(11);
                patientMeal.BedUnitName=cursor.getString(12);
                patientMeal.RoomName=cursor.getString(13);
                patientMeal.BedName=cursor.getString(14);
                patientMeal.PatientFullName=cursor.getString(15);
                patientMeal.ReceptionNo=cursor.getString(16);
                patientMeal.MealTypeName=cursor.getString(17);
                patientMeal.DietType=cursor.getString(18);
                patientMeal.PatientAge=cursor.getInt(19);
                patientMeal.PatientGender=cursor.getString(20);
                patientMeal.MealDate=cursor.getString(21);
                patientMeal.MealType=cursor.getString(22);
                patientMeal.Comment=cursor.getString(23);
                patientMeal.PatientHasMeal=cursor.getInt(24)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.CompanierHasMeal=cursor.getInt(25)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.PatientNutrition=cursor.getString(26);
                patientMeal.DeliveryStatus=cursor.getString(27);
                patientMeal.IsNewDiet=cursor.getInt(28)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.PatientDietOwnerOID=cursor.getInt(29);
                patientMeal.RefDietID=cursor.getInt(30);
                patientMeal.RefDietType=cursor.getString(31);
                patientMeal.RefDietName=cursor.getString(32);
                patientMeal.CompanierName=cursor.getString(33);

                patientMeal.PatientEating=cursor.getInt(34)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.CompanierEating=cursor.getInt(35)==0?Boolean.FALSE :Boolean.TRUE;
                patientMeal.UserID=cursor.getInt(36);
                patientMeal.CreateDate=cursor.getString(37);
                patientMeal.PostedStatus=cursor.getInt(38);
                patientMeal.NewDietTypeID=cursor.getInt(39);
                patientMeal.ErrorMessage=cursor.getString(40);
                patientMeal.CompObjectID=cursor.getString(41);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return patientMeal;
    }
//    public PatientMeal getPatientMealByReceptionNo(String receptionNo) {
//        PatientMeal patientMeal=new PatientMeal();
//        try {
//            Cursor cursor = DATABASE.query(TABLE_CONTACTS, new String[] { KEY_ID,
//                            KEY_PatientDietID,KEY_PatientCompDietID,KEY_DietID,KEY_DietTypeID,KEY_MealTypeID,KEY_ClinicID,KEY_BedUnitID,
//                            KEY_BedID,KEY_ReceptionID,KEY_PatientID,KEY_ClinicName,KEY_BedUnitName,
//                            KEY_RoomName,KEY_BedName,KEY_PatientFullName,KEY_ReceptionNo,KEY_MealTypeName,
//                            KEY_DietType,KEY_PatientAge,KEY_PatientGender,KEY_MealDate,
//                            KEY_MealType,KEY_Comment,KEY_PatientHasMeal,KEY_CompanierHasMeal,KEY_PatientNutrition,
//                            KEY_DeliveryStatus,KEY_IsNewDiet,KEY_PatientDietOwnerOID,KEY_RefDietID,KEY_RefDietType,
//                            KEY_RefDietName,KEY_CompanierName,KEY_PatientEating,KEY_CompanierEating,KEY_UserID,KEY_CreateDate,KEY_PostedStatus}, KEY_ReceptionNo + "=?",
//                    new String[] { String.valueOf(receptionNo) }, null, null, null, null);
//            if (cursor != null && cursor.getCount()>0){
//                cursor.moveToFirst();
//                patientMeal.ID=cursor.getInt(0);
//                patientMeal.PatientDietID=cursor.getInt(1);
//                patientMeal.PatientCompDietID=cursor.getInt(2);
//                patientMeal.DietID=cursor.getInt(3);
//                patientMeal.DietTypeID=cursor.getInt(4);
//                patientMeal.MealTypeID=cursor.getInt(5);
//                patientMeal.ClinicID=cursor.getInt(6);
//                patientMeal.BedUnitID=cursor.getInt(7);
//                patientMeal.BedID=cursor.getInt(8);
//                patientMeal.ReceptionID=cursor.getInt(9);
//                patientMeal.PatientID=cursor.getInt(10);
//                patientMeal.ClinicName=cursor.getString(11);
//                patientMeal.BedUnitName=cursor.getString(12);
//                patientMeal.RoomName=cursor.getString(13);
//                patientMeal.BedName=cursor.getString(14);
//                patientMeal.PatientFullName=cursor.getString(15);
//                patientMeal.ReceptionNo=cursor.getString(16);
//                patientMeal.MealTypeName=cursor.getString(17);
//                patientMeal.DietType=cursor.getString(18);
//                patientMeal.PatientAge=cursor.getInt(19);
//                patientMeal.PatientGender=cursor.getString(20);
//                patientMeal.MealDate=cursor.getString(21);
//                patientMeal.MealType=cursor.getString(22);
//                patientMeal.Comment=cursor.getString(23);
//                patientMeal.PatientHasMeal=cursor.getInt(24)==0?Boolean.FALSE :Boolean.TRUE;
//                patientMeal.CompanierHasMeal=cursor.getInt(25)==0?Boolean.FALSE :Boolean.TRUE;
//                patientMeal.PatientNutrition=cursor.getString(26);
//                patientMeal.DeliveryStatus=cursor.getString(27);
//                patientMeal.IsNewDiet=cursor.getInt(28)==0?Boolean.FALSE :Boolean.TRUE;
//                patientMeal.PatientDietOwnerOID=cursor.getInt(29);
//                patientMeal.RefDietID=cursor.getInt(30);
//                patientMeal.RefDietType=cursor.getString(31);
//                patientMeal.RefDietName=cursor.getString(32);
//                patientMeal.CompanierName=cursor.getString(33);
//
//                patientMeal.PatientEating=cursor.getInt(34)==0?Boolean.FALSE :Boolean.TRUE;
//                patientMeal.CompanierEating=cursor.getInt(35)==0?Boolean.FALSE :Boolean.TRUE;
//                patientMeal.UserID=cursor.getInt(36);
//                patientMeal.CreateDate=cursor.getString(37);
//                patientMeal.PostedStatus=cursor.getInt(38);
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return patientMeal;
//    }
    public List<PatientMeal> getPatientMealListByReceptionNo(String receptionNo){
        List<PatientMeal> patientMeals = new ArrayList<PatientMeal>();
        try {
            String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS +" where " + KEY_ReceptionNo + "=?";

            Cursor cursor = DATABASE.rawQuery(selectQuery, new String[] { String.valueOf(receptionNo) });

            if (cursor != null && cursor.getCount()>0){
                while (cursor.moveToNext()){
                    PatientMeal patientMeal=new PatientMeal();
                    patientMeal.ID=cursor.getInt(0);
                    patientMeal.PatientDietID=cursor.getInt(1);
                    patientMeal.PatientCompDietID=cursor.getInt(2);
                    patientMeal.DietID=cursor.getInt(3);
                    patientMeal.DietTypeID=cursor.getInt(4);
                    patientMeal.MealTypeID=cursor.getInt(5);
                    patientMeal.ClinicID=cursor.getInt(6);
                    patientMeal.BedUnitID=cursor.getInt(7);
                    patientMeal.BedID=cursor.getInt(8);
                    patientMeal.ReceptionID=cursor.getInt(9);
                    patientMeal.PatientID=cursor.getInt(10);
                    patientMeal.ClinicName=cursor.getString(11);
                    patientMeal.BedUnitName=cursor.getString(12);
                    patientMeal.RoomName=cursor.getString(13);
                    patientMeal.BedName=cursor.getString(14);
                    patientMeal.PatientFullName=cursor.getString(15);
                    patientMeal.ReceptionNo=cursor.getString(16);
                    patientMeal.MealTypeName=cursor.getString(17);
                    patientMeal.DietType=cursor.getString(18);
                    patientMeal.PatientAge=cursor.getInt(19);
                    patientMeal.PatientGender=cursor.getString(20);
                    patientMeal.MealDate=cursor.getString(21);
                    patientMeal.MealType=cursor.getString(22);
                    patientMeal.Comment=cursor.getString(23);
                    patientMeal.PatientHasMeal=cursor.getInt(24)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.CompanierHasMeal=cursor.getInt(25)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.PatientNutrition=cursor.getString(26);
                    patientMeal.DeliveryStatus=cursor.getString(27);
                    patientMeal.IsNewDiet=cursor.getInt(28)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.PatientDietOwnerOID=cursor.getInt(29);
                    patientMeal.RefDietID=cursor.getInt(30);
                    patientMeal.RefDietType=cursor.getString(31);
                    patientMeal.RefDietName=cursor.getString(32);
                    patientMeal.CompanierName=cursor.getString(33);

                    patientMeal.PatientEating=cursor.getInt(34)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.CompanierEating=cursor.getInt(35)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.UserID=cursor.getInt(36);
                    patientMeal.CreateDate=cursor.getString(37);
                    patientMeal.PostedStatus=cursor.getInt(38);
                    patientMeal.NewDietTypeID=cursor.getInt(39);
                    patientMeal.ErrorMessage=cursor.getString(40);
                    patientMeals.add(patientMeal);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        // Select All Query

        return patientMeals;
    }
    public List<PatientMeal> getAllPatientMeals() {
        List<PatientMeal> patientMeals = new ArrayList<PatientMeal>();
        try {
            String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS ;

            Cursor cursor = DATABASE.rawQuery(selectQuery, null);

            if (cursor != null && cursor.getCount()>0){
                while (cursor.moveToNext()){
                    PatientMeal patientMeal=new PatientMeal();
                    patientMeal.ID=cursor.getInt(0);
                    patientMeal.PatientDietID=cursor.getInt(1);
                    patientMeal.PatientCompDietID=cursor.getInt(2);
                    patientMeal.DietID=cursor.getInt(3);
                    patientMeal.DietTypeID=cursor.getInt(4);
                    patientMeal.MealTypeID=cursor.getInt(5);
                    patientMeal.ClinicID=cursor.getInt(6);
                    patientMeal.BedUnitID=cursor.getInt(7);
                    patientMeal.BedID=cursor.getInt(8);
                    patientMeal.ReceptionID=cursor.getInt(9);
                    patientMeal.PatientID=cursor.getInt(10);
                    patientMeal.ClinicName=cursor.getString(11);
                    patientMeal.BedUnitName=cursor.getString(12);
                    patientMeal.RoomName=cursor.getString(13);
                    patientMeal.BedName=cursor.getString(14);
                    patientMeal.PatientFullName=cursor.getString(15);
                    patientMeal.ReceptionNo=cursor.getString(16);
                    patientMeal.MealTypeName=cursor.getString(17);
                    patientMeal.DietType=cursor.getString(18);
                    patientMeal.PatientAge=cursor.getInt(19);
                    patientMeal.PatientGender=cursor.getString(20);
                    patientMeal.MealDate=cursor.getString(21);
                    patientMeal.MealType=cursor.getString(22);
                    patientMeal.Comment=cursor.getString(23);
                    patientMeal.PatientHasMeal=cursor.getInt(24)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.CompanierHasMeal=cursor.getInt(25)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.PatientNutrition=cursor.getString(26);
                    patientMeal.DeliveryStatus=cursor.getString(27);
                    patientMeal.IsNewDiet=cursor.getInt(28)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.PatientDietOwnerOID=cursor.getInt(29);
                    patientMeal.RefDietID=cursor.getInt(30);
                    patientMeal.RefDietType=cursor.getString(31);
                    patientMeal.RefDietName=cursor.getString(32);
                    patientMeal.CompanierName=cursor.getString(33);

                    patientMeal.PatientEating=cursor.getInt(34)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.CompanierEating=cursor.getInt(35)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.UserID=cursor.getInt(36);
                    patientMeal.CreateDate=cursor.getString(37);
                    patientMeal.PostedStatus=cursor.getInt(38);
                    patientMeal.NewDietTypeID=cursor.getInt(39);
                    patientMeal.ErrorMessage=cursor.getString(40);
                    patientMeal.CompObjectID=cursor.getString(41);
                    patientMeals.add(patientMeal);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        // Select All Query

        return patientMeals;
    }
    public List<PatientMeal> GetPatientMealListByReceptionNo(String receptionNo) {
        List<PatientMeal> patientMeals = new ArrayList<PatientMeal>();
        try {
            String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS+" WHERE " +KEY_ReceptionNo + " LIKE ? " ;

            Cursor cursor = DATABASE.rawQuery(selectQuery,  new String[] { String.valueOf(receptionNo+"%")});

            if (cursor != null && cursor.getCount()>0){
                while (cursor.moveToNext()){
                    PatientMeal patientMeal=new PatientMeal();
                    patientMeal.ID=cursor.getInt(0);
                    patientMeal.PatientDietID=cursor.getInt(1);
                    patientMeal.PatientCompDietID=cursor.getInt(2);
                    patientMeal.DietID=cursor.getInt(3);
                    patientMeal.DietTypeID=cursor.getInt(4);
                    patientMeal.MealTypeID=cursor.getInt(5);
                    patientMeal.ClinicID=cursor.getInt(6);
                    patientMeal.BedUnitID=cursor.getInt(7);
                    patientMeal.BedID=cursor.getInt(8);
                    patientMeal.ReceptionID=cursor.getInt(9);
                    patientMeal.PatientID=cursor.getInt(10);
                    patientMeal.ClinicName=cursor.getString(11);
                    patientMeal.BedUnitName=cursor.getString(12);
                    patientMeal.RoomName=cursor.getString(13);
                    patientMeal.BedName=cursor.getString(14);
                    patientMeal.PatientFullName=cursor.getString(15);
                    patientMeal.ReceptionNo=cursor.getString(16);
                    patientMeal.MealTypeName=cursor.getString(17);
                    patientMeal.DietType=cursor.getString(18);
                    patientMeal.PatientAge=cursor.getInt(19);
                    patientMeal.PatientGender=cursor.getString(20);
                    patientMeal.MealDate=cursor.getString(21);
                    patientMeal.MealType=cursor.getString(22);
                    patientMeal.Comment=cursor.getString(23);
                    patientMeal.PatientHasMeal=cursor.getInt(24)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.CompanierHasMeal=cursor.getInt(25)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.PatientNutrition=cursor.getString(26);
                    patientMeal.DeliveryStatus=cursor.getString(27);
                    patientMeal.IsNewDiet=cursor.getInt(28)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.PatientDietOwnerOID=cursor.getInt(29);
                    patientMeal.RefDietID=cursor.getInt(30);
                    patientMeal.RefDietType=cursor.getString(31);
                    patientMeal.RefDietName=cursor.getString(32);
                    patientMeal.CompanierName=cursor.getString(33);

                    patientMeal.PatientEating=cursor.getInt(34)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.CompanierEating=cursor.getInt(35)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.UserID=cursor.getInt(36);
                    patientMeal.CreateDate=cursor.getString(37);
                    patientMeal.PostedStatus=cursor.getInt(38);
                    patientMeal.NewDietTypeID=cursor.getInt(39);
                    patientMeal.ErrorMessage=cursor.getString(40);
                    patientMeals.add(patientMeal);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        // Select All Query

        return patientMeals;
    }
    public List<PatientMeal> getPatientMealListByEatingAndStatus() {
        List<PatientMeal> patientMeals = new ArrayList<PatientMeal>();
        try {
            String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS+" WHERE " +KEY_PatientEating+"=1 AND "+KEY_PostedStatus+"!="+ PatientMealPostType.gonderilenHasta ;

            Cursor cursor = DATABASE.rawQuery(selectQuery, null);

            if (cursor != null && cursor.getCount()>0){
                while (cursor.moveToNext()){
                    PatientMeal patientMeal=new PatientMeal();
                    patientMeal.ID=cursor.getInt(0);
                    patientMeal.PatientDietID=cursor.getInt(1);
                    patientMeal.PatientCompDietID=cursor.getInt(2);
                    patientMeal.DietID=cursor.getInt(3);
                    patientMeal.DietTypeID=cursor.getInt(4);
                    patientMeal.MealTypeID=cursor.getInt(5);
                    patientMeal.ClinicID=cursor.getInt(6);
                    patientMeal.BedUnitID=cursor.getInt(7);
                    patientMeal.BedID=cursor.getInt(8);
                    patientMeal.ReceptionID=cursor.getInt(9);
                    patientMeal.PatientID=cursor.getInt(10);
                    patientMeal.ClinicName=cursor.getString(11);
                    patientMeal.BedUnitName=cursor.getString(12);
                    patientMeal.RoomName=cursor.getString(13);
                    patientMeal.BedName=cursor.getString(14);
                    patientMeal.PatientFullName=cursor.getString(15);
                    patientMeal.ReceptionNo=cursor.getString(16);
                    patientMeal.MealTypeName=cursor.getString(17);
                    patientMeal.DietType=cursor.getString(18);
                    patientMeal.PatientAge=cursor.getInt(19);
                    patientMeal.PatientGender=cursor.getString(20);
                    patientMeal.MealDate=cursor.getString(21);
                    patientMeal.MealType=cursor.getString(22);
                    patientMeal.Comment=cursor.getString(23);
                    patientMeal.PatientHasMeal=cursor.getInt(24)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.CompanierHasMeal=cursor.getInt(25)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.PatientNutrition=cursor.getString(26);
                    patientMeal.DeliveryStatus=cursor.getString(27);
                    patientMeal.IsNewDiet=cursor.getInt(28)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.PatientDietOwnerOID=cursor.getInt(29);
                    patientMeal.RefDietID=cursor.getInt(30);
                    patientMeal.RefDietType=cursor.getString(31);
                    patientMeal.RefDietName=cursor.getString(32);
                    patientMeal.CompanierName=cursor.getString(33);

                    patientMeal.PatientEating=cursor.getInt(34)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.CompanierEating=cursor.getInt(35)==0?Boolean.FALSE :Boolean.TRUE;
                    patientMeal.UserID=cursor.getInt(36);
                    patientMeal.CreateDate=cursor.getString(37);
                    patientMeal.PostedStatus=cursor.getInt(38);
                    patientMeal.NewDietTypeID=cursor.getInt(39);
                    patientMeal.ErrorMessage=cursor.getString(40);
                    patientMeal.CompObjectID=cursor.getString(41);
                    patientMeals.add(patientMeal);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        // Select All Query

        return patientMeals;
    }
    public int updatePatientMeal(PatientMeal patientMeal) {
        try {
            ContentValues values = new ContentValues();

            values.put(KEY_PatientDietID, patientMeal.PatientDietID);
            values.put(KEY_PatientCompDietID, patientMeal.PatientCompDietID);
            values.put(KEY_DietID, patientMeal.DietID);
            values.put(KEY_DietTypeID, patientMeal.DietTypeID);
            values.put(KEY_MealTypeID, patientMeal.MealTypeID);
            values.put(KEY_ClinicID, patientMeal.ClinicID);
            values.put(KEY_BedUnitID, patientMeal.BedUnitID);
            values.put(KEY_BedID, patientMeal.BedID);
            values.put(KEY_ReceptionID, patientMeal.ReceptionID);
            values.put(KEY_BedUnitID, patientMeal.BedUnitID);
            values.put(KEY_PatientID, patientMeal.PatientID);
            values.put(KEY_ClinicName, patientMeal.ClinicName);
            values.put(KEY_BedUnitName, patientMeal.BedUnitName);
            values.put(KEY_RoomName, patientMeal.RoomName);
            values.put(KEY_BedName, patientMeal.BedName);
            values.put(KEY_PatientFullName, patientMeal.PatientFullName);
            values.put(KEY_ReceptionNo, patientMeal.ReceptionNo);
            values.put(KEY_MealTypeName, patientMeal.MealTypeName);
            values.put(KEY_DietType, patientMeal.DietType);
            values.put(KEY_PatientAge, patientMeal.PatientAge);
            values.put(KEY_PatientGender, patientMeal.PatientGender);
            values.put(KEY_MealDate, patientMeal.MealDate);
            values.put(KEY_MealType, patientMeal.MealType);
            values.put(KEY_Comment, patientMeal.Comment);
            values.put(KEY_PatientHasMeal, patientMeal.PatientHasMeal?1:0);
            values.put(KEY_CompanierHasMeal, patientMeal.CompanierHasMeal?1:0);
            values.put(KEY_PatientNutrition, patientMeal.PatientNutrition);
            values.put(KEY_DeliveryStatus, patientMeal.DeliveryStatus);
            values.put(KEY_IsNewDiet, patientMeal.IsNewDiet?1:0);
            values.put(KEY_PatientDietOwnerOID, patientMeal.PatientDietOwnerOID);
            values.put(KEY_RefDietID, patientMeal.RefDietID);
            values.put(KEY_RefDietType, patientMeal.RefDietType);
            values.put(KEY_RefDietName, patientMeal.RefDietName);
            values.put(KEY_CompanierName, patientMeal.CompanierName);

            values.put(KEY_PatientEating, patientMeal.PatientEating?1:0);
            values.put(KEY_CompanierEating, patientMeal.CompanierEating?1:0);
            values.put(KEY_UserID, patientMeal.UserID);
            values.put(KEY_CreateDate, patientMeal.CreateDate);
            values.put(KEY_PostedStatus, patientMeal.PostedStatus);
            values.put(KEY_NewDietTypeID, patientMeal.NewDietTypeID);
            values.put(KEY_ErrorMessage, patientMeal.ErrorMessage);
            values.put(KEY_CompObjectID, patientMeal.CompObjectID);


            Integer r=DATABASE.update(TABLE_CONTACTS, values, KEY_PatientCompDietID + " = "+patientMeal.PatientCompDietID,null );
            // updating row
            return r;
        }catch (Exception e){
            return -1;
        }

    }

    public void deletePatientMeal(PatientMeal patientMeal) {

        int result= DATABASE.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(patientMeal.ID) });

    }
    public void deletePatientMeal(Integer receptionId) {
        DATABASE.delete(TABLE_CONTACTS, KEY_ReceptionID + " = ?",
                new String[] { String.valueOf(receptionId) });
        DATABASE.close();
    }

    public int getDataCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;

        Cursor cursor = DATABASE.rawQuery(countQuery, null);
        Integer count=0;
        if (cursor!=null && cursor.getCount()>0){
            count=cursor.getCount();
        }
        cursor.close();
        return  count;
    }
    public int getDataCount(Integer status) {
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS + " where "+KEY_PostedStatus+"="+status.toString();

        Cursor cursor = DATABASE.rawQuery(countQuery, null);

        Integer count=0;
        if (cursor!=null && cursor.getCount()>0){
            count=cursor.getCount();
        }
        cursor.close();
        return  count;
    }
    public int getPatientMealDataCount(Integer patientCompDietID) {
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS + " where "+KEY_PatientCompDietID+"="+patientCompDietID.toString();

        Cursor cursor = DATABASE.rawQuery(countQuery, null);

        Integer count=0;
        if (cursor!=null && cursor.getCount()>0){
            count=cursor.getCount();
        }
        cursor.close();
        return  count;
    }

}