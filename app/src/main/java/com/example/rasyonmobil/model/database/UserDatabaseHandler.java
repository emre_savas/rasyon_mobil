package com.example.rasyonmobil.model.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import com.example.rasyonmobil.model.User;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class UserDatabaseHandler {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "User";
    private static String TABLE_NAME = "users";
    private static final String KEY_ID = "id";
    private static final String KEY_USER_NAME = "Username";
    private static final String KEY_PASSWORD = "Password";
    private static final String KEY_USERID = "UserID";
    private static final String KEY_TITLE = "Title";
    private static final String KEY_PID = "PID";
    private static final String KEY_PERSONNELNAME = "PersonnelName";
    private static final String KEY_PERSONNELID = "PersonnelID";
    private static SQLiteDatabase DATABASE=null;
    public UserDatabaseHandler(Context context) {
        DATABASE=context.openOrCreateDatabase(DATABASE_NAME,MODE_PRIVATE,null);
        //3rd argument to be passed is CursorFactory instance
        onCreate(DATABASE);
    }

    public  void setTableName(String TableName){
        TABLE_NAME=TableName;
    }

    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE Table if not exists "+TABLE_NAME+" (id INTEGER PRIMARY KEY ,"+KEY_USER_NAME+" VARCHAR,"+KEY_PASSWORD+" VARCHAR," +
                ""+KEY_USERID+" VARCHAR,"+KEY_TITLE+" VARCHAR,"+KEY_PID+" VARCHAR,"+KEY_PERSONNELNAME+" VARCHAR,"+KEY_PERSONNELID+" INTEGER)";
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade() {
        DATABASE.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(DATABASE);
    }

    public void addUser(User user) {

        try {
            String sqlString="INSERT INTO "+TABLE_NAME+" ("+KEY_USER_NAME+","+KEY_PASSWORD+","+KEY_USERID+","+KEY_TITLE+","+KEY_PID+","+KEY_PERSONNELNAME+","+KEY_PERSONNELID+") VALUES (?,?,?,?,?,?,?)";
            SQLiteStatement sqLiteStatement=DATABASE.compileStatement(sqlString);
            sqLiteStatement.bindString(1,(user.UserName==null || user.UserName=="")?" ":user.UserName.toLowerCase());
            sqLiteStatement.bindString(2,(user.Password==null || user.Password=="")?" ":user.Password);
            sqLiteStatement.bindDouble(3,user.UserID);
            sqLiteStatement.bindString(4,(user.Title==null || user.Title=="")?" ":user.Title);
            sqLiteStatement.bindString(5,(user.PID==null || user.PID=="")?" ":user.PID);
            sqLiteStatement.bindString(6,(user.PersonnelName==null || user.PersonnelName=="")?" ":user.PersonnelName);
            sqLiteStatement.bindDouble(7,user.PersonnelID);
            sqLiteStatement.execute();
        }catch (Exception e){
            e.printStackTrace();
            Log.i("PatientFoods","tabloya veri güncellemeda hata ! "+e.getMessage());
        }

    }

    public User getUser(String userName) {
        User user=new User();
        try {
            Cursor cursor = DATABASE.query(TABLE_NAME, new String[] { KEY_ID,
                            KEY_USER_NAME, KEY_PASSWORD ,KEY_USERID,KEY_TITLE,KEY_PID,KEY_PERSONNELNAME,KEY_PERSONNELID}, KEY_USER_NAME + "=?",
                    new String[] { userName.toLowerCase() }, null, null, null, null);
            if (cursor != null && cursor.getCount()>0 ){
                cursor.moveToFirst();
                int idIx=cursor.getColumnIndex(KEY_ID);
                int nameIx=cursor.getColumnIndex(KEY_USER_NAME);
                int paswordIx=cursor.getColumnIndex(KEY_PASSWORD);
                int userIdIx=cursor.getColumnIndex(KEY_USERID);
                int titleIx=cursor.getColumnIndex(KEY_TITLE);
                int pidIx=cursor.getColumnIndex(KEY_PID);
                int personelNameIx=cursor.getColumnIndex(KEY_PERSONNELNAME);
                int personelIdIx=cursor.getColumnIndex(KEY_PERSONNELID);

                user.Id=cursor.getInt(idIx);
                user.UserName=cursor.getString(nameIx)==null?" ":cursor.getString(nameIx);
                user.Password=cursor.getString(paswordIx)==null?" ":cursor.getString(paswordIx);
                user.UserID=cursor.getInt((userIdIx));
                user.Title=cursor.getString(titleIx)==null?" ":cursor.getString(titleIx);
                user.PID=cursor.getString(pidIx)==null?" ":cursor.getString(pidIx);
                user.PersonnelName=cursor.getString(personelNameIx)==null?" ":cursor.getString(personelNameIx);
                user.PersonnelID=cursor.getInt(personelIdIx);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return user;
    }

    public List<User> getAllUsers() {
        List<User> userList = new ArrayList<User>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        Cursor cursor = DATABASE.rawQuery(selectQuery, null);

        int idIx=cursor.getColumnIndex(KEY_ID);
        int nameIx=cursor.getColumnIndex(KEY_USER_NAME);
        int paswordIx=cursor.getColumnIndex(KEY_PASSWORD);
        int userIdIx=cursor.getColumnIndex(KEY_USERID);
        int titleIx=cursor.getColumnIndex(KEY_TITLE);
        int pidIx=cursor.getColumnIndex(KEY_PID);
        int personelNameIx=cursor.getColumnIndex(KEY_PERSONNELNAME);
        int personelIdIx=cursor.getColumnIndex(KEY_PERSONNELID);

        // looping through all rows and adding to list
        try {
            if (cursor != null && cursor.getCount()>0) {
                while (cursor.moveToNext()){
                    User user=new User();
                    user.Id=cursor.getInt(idIx);
                    user.UserName=cursor.getString(nameIx)==null?" ":cursor.getString(nameIx);
                    user.Password=cursor.getString(paswordIx)==null?" ":cursor.getString(paswordIx);
                    user.UserID=cursor.getInt((userIdIx));
                    user.Title=cursor.getString(titleIx)==null?" ":cursor.getString(titleIx);
                    user.PID=cursor.getString(pidIx)==null?" ":cursor.getString(pidIx);
                    user.PersonnelName=cursor.getString(personelNameIx)==null?" ":cursor.getString(personelNameIx);
                    user.PersonnelID=cursor.getInt(personelIdIx);
                    userList.add(user);
                }
            }

        }catch (Exception e){

        }


        return userList;
    }

//    public int updateContact(User user) {
//        ContentValues values = new ContentValues();
//        values.put(KEY_USER_NAME, user.UserName);
//        values.put(KEY_PASSWORD, user.Password);
//
//        // updating row
//        return DATABASE.update(TABLE_CONTACTS, values, KEY + " = ?",
//                new String[] { String.valueOf(exampleObject.receptionId) });
//    }

//    public void deleteContact(ExampleObject contact) {
//
//        DATABASE.delete(TABLE_CONTACTS, KEY_ID + " = ?",
//                new String[] { String.valueOf(contact.receptionId) });
//        DATABASE.close();
//    }
//    public void deleteContact(Integer receptionId) {
//        DATABASE.delete(TABLE_CONTACTS, KEY_ID + " = ?",
//                new String[] { String.valueOf(receptionId) });
//        DATABASE.close();
//    }

    public int getDataCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;

        Cursor cursor = DATABASE.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

}