package com.example.rasyonmobil.network;

import com.example.rasyonmobil.model.DietType;
import com.example.rasyonmobil.model.PatientMeal;
import com.example.rasyonmobil.model.TokenObject;
import com.example.rasyonmobil.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface IRasyonService {
    @FormUrlEncoded
    @POST("token")
    Call<TokenObject> GetToken(@Field("username") String username, @Field("password") String password , @Field("grant_type") String grant_type);
    @POST("api/mobile/getmeallist")
    Call<List<PatientMeal>> GetMealList( @Query("date") String date);
    @POST("api/mobile/deliverMeal")
    Call<String> DeliverMeal(@Body PatientMeal meal);
    @POST("api/mobile/getuserlist")
    Call<List<User>> GetUserList();
    @POST("api/mobile/getDietTypeList")
    Call<List<DietType>> GetDietTypeList();
    @GET("api/mobile/healthcheck")
    Call<String> healthcheck();
}
