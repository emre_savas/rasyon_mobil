package com.example.rasyonmobil.ui.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.rasyonmobil.BuildConfig;
import com.example.rasyonmobil.R;
import com.example.rasyonmobil.backbone.MyBaseActivity;
import com.example.rasyonmobil.backbone.MyBuild;
import com.example.rasyonmobil.engines.PatientMealPostType;
import com.example.rasyonmobil.engines.SharedPrefencesEngine;
import com.example.rasyonmobil.model.PatientMeal;
import com.example.rasyonmobil.model.ServiceConfig;
import com.example.rasyonmobil.model.User;
import com.example.rasyonmobil.network.IRasyonService;
import com.example.rasyonmobil.util.DataBaseOperation;
import com.example.rasyonmobil.util.Util;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;

public class LoginActivity  extends MyBaseActivity {
    EditText etUsername, etPassword;
    TextView versText;
    ImageView setting;
    FrameLayout fl_login_btn;
    DataBaseOperation dataBaseOperation;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //autoRefleshData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        versText.setText("v" + BuildConfig.VERSION_NAME);
        settingConfig();
        dataBaseOperation = new DataBaseOperation(this,"");
        //dataBaseOperation.setPatientMealDatabaseHandlerUpgrade();
//
//
////        if (spref().getbooleanPreferences(SharedPrefencesEngine.IS_LOGGED_IN)) {
////            startActivity(new Intent(SplashActivty.this, PatientQueryActivity.class));
////            finish();
////        }
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(LoginActivity.this, SettingsActivty.class);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
        }
        autoRefleshData();
    }

    public void settingConfig(){
        if (MyBuild.isOnDevelopment()) {
//            spref().savePreferences(SharedPrefencesEngine.IN_IP, "185.48.181.40");
//            spref().savePreferences(SharedPrefencesEngine.OUT_IP, "185.48.181.40");
            spref().savePreferences(SharedPrefencesEngine.IN_IP, "10.211.65.138");
            spref().savePreferences(SharedPrefencesEngine.OUT_IP, "10.211.65.138");
//            spref().savePreferences(SharedPrefencesEngine.IN_IP, "192.168.1.10");
//            spref().savePreferences(SharedPrefencesEngine.OUT_IP, "192.168.1.10");
            spref().savePreferences(SharedPrefencesEngine.WIFI_NAME, "emre");
            etUsername.setText("sarus");
            etPassword.setText("06Gop34*");
//            etPassword.setText("1");
        }
        else {
            ServiceConfig config = getServiceConfigFromFile();
            if (config != null) {
                setting.setVisibility(View.GONE);
                spref().savePreferences(SharedPrefencesEngine.IN_IP, config.ic_ip);
                spref().savePreferences(SharedPrefencesEngine.OUT_IP, config.dis_ip);
                spref().savePreferences(SharedPrefencesEngine.WIFI_NAME, config.tesis_wifi);
                spref().savePreferences(SharedPrefencesEngine.TESIS_ADI, config.tesis_adi);
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    settingConfig();
                } else {
                }
                break;
            }
        }
    }

    public void LoginControl(View view) {
        closeKeyBoard();
        if (etUsername.getText().toString().matches("")) {
            etUsername.setError("Bu alan boş geçilemez!");
        }

        if (etPassword.getText().toString().matches("")) {
            etPassword.setError("Bu alan boş geçilemez!");
            return;
        }
        //if (checkSettings()) {
        //  return;
        //}

        Util.openProgressDialog(this, "Giriş...").show();
        final String uName = etUsername.getText().toString();
        final String pass = etPassword.getText().toString();
        User user = dataBaseOperation.GetUserByUserName(uName);
        if (user == null || user.UserName == null) {
            Toast.makeText(this, "Kullanıcı Bulunamadı !",
                    Toast.LENGTH_LONG).show();

            Util.cancelDialog();
            return;
        }
        if (user != null && user.Password.equals(pass)) {
            spref().savePreferences(SharedPrefencesEngine.USER_ID, user.UserID);
            spref().savePreferences(SharedPrefencesEngine.USER_NAME, user.UserName);
            spref().savePreferences(SharedPrefencesEngine.USER_TITLE, user.Title);
            spref().savePreferences(SharedPrefencesEngine.PID, user.PID);
            spref().savePreferences(SharedPrefencesEngine.IS_LOGGED_IN, true);
            spref().savePreferences(SharedPrefencesEngine.USER_LOGIN_PASSWORD, pass);
            spref().savePreferences(SharedPrefencesEngine.USER_LOGIN_USERNAME, uName);
            Util.cancelDialog();
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Şifreniz kontrol ediniz !",
                    Toast.LENGTH_LONG).show();

            Util.cancelDialog();
            return;
        }
        Util.cancelDialog();
    }

    public void initView() {

        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        fl_login_btn = findViewById(R.id.fl_login_btn);
        versText = findViewById(R.id.versText);
        setting = findViewById(R.id.setting);


//        fl_login_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (etUsername.getText().toString().matches("")) {
//                    etUsername.setError("Bu alan boş geçilemez!");
//                }
//
//                if (etPassword.getText().toString().matches("")) {
//                    etPassword.setError("Bu alan boş geçilemez!");
//                    return;
//                }
//                if (checkSettings()) {
//                    return;
//                }
//                getToken();
//            }
//        });
    }


    public void closeKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etUsername.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(etPassword.getWindowToken(), 0);
    }

    private boolean checkSettings() {
        if (spref().getStringPreferences(SharedPrefencesEngine.IN_IP).equals("empty")
                || spref().getStringPreferences(SharedPrefencesEngine.OUT_IP).equals("empty")
                || spref().getStringPreferences(SharedPrefencesEngine.WIFI_NAME).equals("empty")) {
            showDialogAboutSettings();
            return true;
        } else {
            return false;
        }
    }

    private void showDialogAboutSettings() {
        new MaterialDialog.Builder(this)
                .title("Hata")
                .content("Uygulamanın çalışabilmesi için ayarlar ekranından iç ve dış IP adres bilgilerini girmelisiniz.")
                .positiveText("TAMAM")
                .show();
    }

    public ServiceConfig getServiceConfigFromFile() {

        ServiceConfig config = null;
        File sdcard = Environment.getExternalStorageDirectory();

        File file = new File(sdcard, "service_config.txt");
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (!text.toString().equals(""))
                config = new Gson().fromJson(text.toString(), ServiceConfig.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return config;
    }

    public void refreshData(View view) {
        Util.openProgressDialog(this, "Güncelleniyor...").show();
        Log.i("progress","geldi");
//        if (dataBaseOperation.GetPatientMealCountByStatus(PatientMealPostType.hataliHasta) > 0 && dataBaseOperation.GetPatientMealCountByStatus(PatientMealPostType.gonderilemeyenHasta) > 0)//gönderilmeyen ve hatalı olanlar
//        {
//            Util.cancelDialog();
//            Toast.makeText(this, "Hasta Listesi Güncellenemez", Toast.LENGTH_LONG).show();
//        } else {
            dataBaseOperation.UserDBReflesh();
//            dataBaseOperation.PatientMealDBReflesh();
//            dataBaseOperation.DietTypeDBReflesh();

//        }
//        dataBaseOperation.PatientMealDBReflesh();
//        dataBaseOperation.UserDBReflesh();

    }
    public void autoRefleshData(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;

        if (connected){
            refreshData(setting);
        }
    }
    public void testClick(View view){
        dataBaseOperation.TestService();
    }
}