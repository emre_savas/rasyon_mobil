package com.example.rasyonmobil.ui.activities;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rasyonmobil.R;
import com.example.rasyonmobil.backbone.MyBaseActivity;
import com.example.rasyonmobil.engines.PatientMealPostType;
import com.example.rasyonmobil.engines.SharedPrefencesEngine;
import com.example.rasyonmobil.model.DietType;
import com.example.rasyonmobil.model.PatientMeal;
import com.example.rasyonmobil.model.database.PatientMealDatabaseHandler;
import com.example.rasyonmobil.ui.adapters.DietTypeAdapter;
import com.example.rasyonmobil.util.DataBaseOperation;
import com.example.rasyonmobil.util.Util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MainActivity extends MyBaseActivity {

    TextView hastaAdiText, clinicNameText, roomBedNoText, ogunAdiText;
    EditText searchEditText, refakatciText;
    PatientMealDatabaseHandler patientMealDatabaseHandler;
    PatientMeal patientMeal;
    //   Switch refekatciSwich;
    Spinner spinner;
    DietTypeAdapter dietTypeAdapter;
    List<DietType> dietTypeList;
    DataBaseOperation dataBaseOperation;
    ConstraintLayout hastaDetayLayout, barcodeLayout;
    ImageView checkImage;
    ImageView iv_ara_ogun_getir;
    CheckBox yeniKayitCheck;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main2_layout);
        initView();
        dataBaseOperation = new DataBaseOperation(this, SettingHospital());


        searchEditText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                closeKeyPad(v);
                String receptionNo = searchEditText.getText() + "";
                if (dataBaseOperation.FindMeal(false) == "ogun disi") {
                    Toast.makeText(MainActivity.this, "Yemek verme zamanı gelmedi !", Toast.LENGTH_LONG).show();
                } else if (receptionNo.length() > 7) {
//                    patientMeal =dataBaseOperation.GetPatientMealByReceptionNo(temp);
                    String[] temp = receptionNo.split("-");
                    patientMeal = dataBaseOperation.getPatientMealByReceptionNoNotDash(temp[0], false);
                    if (patientMeal.ID > 0) {
                        setPatientInformation();

                        return true;
                    } else {
                        hastaOgunEkle();
//                        Toast.makeText(MainActivity.this, "Hasta Bulunamadı !", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                }
                return false;
            }
        });
        refakatciText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                closeKeyPad(v);
                String value = refakatciText.getText().toString();
                refakatciText.setError(null);
//                int objectId;
//                try {
//                    objectId = Integer.parseInt();
//                }
//                catch (NumberFormatException e)
//                {
//                    objectId = 0;
//                }
                if (value.length() < 3)
                    return false;
                if (patientMeal.CompObjectID.equals("0")) {
                    refakatciText.setError("Refakatçi bilgisi alınamadı !");
                    Toast.makeText(MainActivity.this, "Refakatçi bilgisi alınamadı !", Toast.LENGTH_LONG).show();
                    return false;
                }

                if (value.equals(patientMeal.CompObjectID)) {
                    patientMeal.CompanierEating = true;
                } else {
                    patientMeal.CompanierEating = false;
                    refakatciText.setError("Refakatçi eşleşmesi sağlanamadı !");
                }
                return false;
            }
        });
//        dataBaseOperation.test();
//        refekatciSwich.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (patientMeal != null) {
//                    if (isChecked) {
//                        refakatciText.setVisibility(View.VISIBLE);
//                    } else {
//                        refakatciText.setVisibility(View.INVISIBLE);
//                    }
//                }
//            }
//        });
        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        if (id != null) {
            patientMeal = patientMealDatabaseHandler.getPatientMeal(id);
            setPatientInformation();
        }

    }

    public void hastaOgunEkle() {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setTitle("Uyarı");
        alertBuilder.setMessage("Hasta öğün kaydı bulunmamaktadır. Eklemek istiyor musunuz ?")
                .setCancelable(false)
//                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton("Evet", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        patientMeal = new PatientMeal();
                        patientMeal.ReceptionNo = searchEditText.getText() + "";
                        patientMeal.ReceptionID = -1;
                        patientMeal.PatientHasMeal = false;
                        patientMeal.CompanierHasMeal = false;
                        patientMeal.IsNewDiet = false;
                        patientMeal.PatientEating = false;
                        patientMeal.CompanierEating = false;
                        patientMeal.UserID = 0;
                        patientMeal.NewDietTypeID = 0;
                        patientMeal.PostedStatus = PatientMealPostType.yeniHasta;
                        patientMeal.PatientFullName = "Yeni Kayıt";
                        patientMeal.BedUnitName = "-";
                        patientMeal.RoomName = "-";
                        patientMeal.BedName = "-";
                        patientMeal.MealTypeName = dataBaseOperation.FindMeal(false);
                        setPatientInformation();
                        dialog.dismiss();
                        //Toast.makeText(MainActivity.this, "Hasta Eklendi !", Toast.LENGTH_SHORT).show();

                    }
                }).setNegativeButton("Hayır", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    public void refleshScreen(View view) {
        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        startActivity(intent);
        finish();

    }

    public void save(View view) {
        Util.openProgressDialog(this, "Kaydediliyor...").show();

        int result = SavePatientMeal();
        if (result > 0) {
            clearScreen();
        }
        Util.cancelDialog();

    }

    public void closeKeyPad(View view){
//        if (view != null) {
//            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//        }
    }

    public int SavePatientMeal() {
        if (patientMeal == null && patientMeal.ReceptionID <= 0) {
            Toast.makeText(this, "Hasta Seçmelisiniz !", Toast.LENGTH_LONG).show();
            return 0;
        }
        if (patientMeal.NewDietTypeID <= 0 && patientMeal.DietTypeID <= 0) {
            Toast.makeText(this, "Hasta öğün bilgisini seçmelisiniz !", Toast.LENGTH_LONG).show();
            return 0;
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        patientMeal.PatientEating = true;
        patientMeal.UserID = spref().getIntPreferences(SharedPrefencesEngine.USER_ID);
        patientMeal.CreateDate = dateFormat.format(date);
        if (refakatciText.getText().toString().equals(patientMeal.CompObjectID)) {
            patientMeal.CompanierEating = true;
        }else{
            patientMeal.CompanierEating = false;
        }
        patientMeal.CompanierName = refakatciText.getText().toString();
        patientMeal.NewDietTypeID = patientMeal.NewDietTypeID == patientMeal.DietTypeID ? 0 : patientMeal.NewDietTypeID;
        int result = 0;
        if (patientMeal.ID > 0) {
            result = patientMealDatabaseHandler.updatePatientMeal(patientMeal);
        } else {
            result = dataBaseOperation.AddPatientMeal(patientMeal);
        }
        if (result > 0) {
            Toast.makeText(this, "Hasta bilgileri Kaydedildi.", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Hasta bilgileri Kaydedilirken hata oluştu.", Toast.LENGTH_LONG).show();
        }
        return result;
    }

    public void clearScreen() {
        searchEditText.setText("");
        patientMeal = null;
        barcodeLayout.setVisibility(View.VISIBLE);
        iv_ara_ogun_getir.setVisibility(View.INVISIBLE);
        hastaDetayLayout.setVisibility(View.INVISIBLE);
    }

    public void startSetData(View view) {
        Util.openProgressDialog(this, "Hasta verileri yükleniyor...").show();
        Intent intent = new Intent(MainActivity.this, PatientMealList.class);
        //intent.putExtra("info","new");
        startActivity(intent);
        Util.cancelDialog();
        finish();
    }

    public void Logout(View view) {
        Util.openProgressDialog(this, "Çıkış yapılıyor...").show();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        //intent.putExtra("info","new");
        startActivity(intent);
        Util.cancelDialog();
        finish();
    }

    public void AraOgunGetir(View view) {
        SavePatientMeal();
        String receptionNo = searchEditText.getText() + "";
        if (dataBaseOperation.FindMeal(true) == "ogun disi") {
            Toast.makeText(MainActivity.this, "Yemek verme zamanı gelmedi !", Toast.LENGTH_LONG).show();
        } else if (receptionNo.length() > 6) {
            patientMeal = dataBaseOperation.getPatientMealByReceptionNoNotDash(receptionNo, true);
            if (patientMeal.ID > 0) {
                setPatientInformation();
            } else {
                hastaOgunEkle();
                //Toast.makeText(MainActivity.this, "Ara Öğün Bulunamadı !", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void setPatientInformation() {
        if (patientMeal.PatientEating) {
            Toast.makeText(this, "Hastaya yemek teslim edilmiştir.", Toast.LENGTH_LONG).show();
            checkImage.setVisibility(View.VISIBLE);
        } else {
            checkImage.setVisibility(View.INVISIBLE);
        }
        if (SettingHospital() == "GOP") {
            if (patientMeal.MealTypeName.matches("Akşam")) {
                iv_ara_ogun_getir.setVisibility(View.VISIBLE);
            } else {
                iv_ara_ogun_getir.setVisibility(View.INVISIBLE);
            }
        } else if (SettingHospital() == "Taksim") {
            if (patientMeal.MealTypeName.matches("(.*)Ara(.*)")) {
                iv_ara_ogun_getir.setVisibility(View.INVISIBLE);
            } else {
                iv_ara_ogun_getir.setVisibility(View.VISIBLE);
            }
        }


        yeniKayitCheck.setChecked(patientMeal.PatientCompDietID == 0 ? true : false);
        searchEditText.setText(patientMeal.ReceptionNo);
        hastaAdiText.setText(patientMeal.PatientFullName);
        clinicNameText.setText(patientMeal.BedUnitName);
        roomBedNoText.setText(patientMeal.RoomName + "/" + patientMeal.BedName);
        ogunAdiText.setText(patientMeal.MealTypeName + " / ");
        if (patientMeal.CompanierEating) {
            refakatciText.setText(patientMeal.CompanierName);
        } else {
            refakatciText.setText("");
        }
        //Toast.makeText(MainActivity.this, patientMeal.CompanierHasMeal ? "True" : "False", Toast.LENGTH_LONG).show();

        if (patientMeal.CompanierHasMeal != null && !patientMeal.CompanierHasMeal)
//            Toast.makeText(this,"Refakatçi yemeği vermemeniz önerilir !.",Toast.LENGTH_LONG).show();
            refakatciText.setVisibility(View.GONE);
        else {
            refakatciText.setVisibility(View.VISIBLE);
            refakatciText.setFocusable(true);
            refakatciText.setFocusableInTouchMode(true);
            refakatciText.requestFocus();
            Toast.makeText(MainActivity.this, "Refakatçi yemeği bulunmaktadır. Barkodunu Okutunuz !", Toast.LENGTH_LONG).show();
//            Toast.makeText(MainActivity.this, String.valueOf(patientMeal.CompObjectID), Toast.LENGTH_LONG).show();

        }
//            Toast.makeText(this,"Refakatçi yemeği verebilirsiniz.",Toast.LENGTH_LONG).show();

        try {
            dietTypeList = dataBaseOperation.GetDietTypeList();
            dietTypeAdapter = new DietTypeAdapter(MainActivity.this, dietTypeList);
            spinner.setAdapter(dietTypeAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view,
                                           int position, long id) {
                    // Here you get the current item (a User object) that is selected by its position
                    DietType dietType = dietTypeAdapter.getItem(position);
                    try {
                        //  Toast.makeText(MainActivity.this, dietType.Name + " */* " + dietType.DietType, Toast.LENGTH_LONG).show();

                        patientMeal.NewDietTypeID = dietType.ObjectID;
                        patientMeal.DietType = dietType.DietType;
                        patientMeal.MealType = dietType.Name;
//                        patientMeal.DietType=dietType.Name;
//                        patientMeal.MealType=dietType.DietType;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    // Here you can do the action you want to...
//                Toast.makeText(MainActivity.this, "ID: " + dietType.AraOgun + "\nName: " + dietType.Name,
////                        Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapter) {
                }
            });
            int index = 0;
            for (DietType dietType : dietTypeList) {
                if (patientMeal.NewDietTypeID > 0 && patientMeal.NewDietTypeID == dietType.ObjectID) {
                    break;
                } else if (patientMeal.DietTypeID == dietType.ObjectID && patientMeal.NewDietTypeID <= 0) {
                    break;
                }
                index++;
            }
            if (dietTypeList.size() == index)
                index = 0;
            spinner.setSelection(index);
        } catch (Exception e) {
            e.printStackTrace();
        }

        barcodeLayout.setVisibility(View.INVISIBLE);
        hastaDetayLayout.setVisibility(View.VISIBLE);

    }

    public void initView() {
        yeniKayitCheck = findViewById(R.id.yeniKayitCheck);
        checkImage = findViewById(R.id.checkImage);
        spinner = findViewById(R.id.spinner);
        refakatciText = findViewById(R.id.refakatciText);
        hastaAdiText = findViewById(R.id.patientNameText);
        clinicNameText = findViewById(R.id.clinicNameText);
        roomBedNoText = findViewById(R.id.roomBedNoText);
        searchEditText = findViewById(R.id.searchEditText);
        ogunAdiText = findViewById(R.id.ogunAdiText);
        iv_ara_ogun_getir = findViewById(R.id.iv_ara_ogun_getir);
        barcodeLayout = findViewById(R.id.barcodeLayout);
        hastaDetayLayout = findViewById(R.id.hastaDetayLayout);
        patientMealDatabaseHandler = new PatientMealDatabaseHandler(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {//hangi menü göstereceğimizi

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.veri_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {//hangi item seçildiğinde

//        if (item.getItemId()==R.id.add_art_veri_cek){
////            Intent intent=new Intent(MainActivity.this,LoginActivity.class);
////            //intent.putExtra("info","new");
////            startActivity(intent);
//
//        }else
        if (item.getItemId() == R.id.add_art_veri_gonder) {
            Intent intent = new Intent(MainActivity.this, PatientMealList.class);
            //intent.putExtra("info","new");
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public String SettingHospital() {
        String ip = spref().getStringPreferences(SharedPrefencesEngine.IN_IP);
        if (ip.matches("10.211.65.138")) {
            return "GOP";
        } else {
            return "Taksim";
        }
    }
}
