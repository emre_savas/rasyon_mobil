package com.example.rasyonmobil.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rasyonmobil.R;
import com.example.rasyonmobil.backbone.MyBaseActivity;
import com.example.rasyonmobil.backbone.MyBuild;
import com.example.rasyonmobil.engines.PatientMealPostType;
import com.example.rasyonmobil.model.PatientMeal;
import com.example.rasyonmobil.ui.adapters.PatientMealAdapter;
import com.example.rasyonmobil.util.DataBaseOperation;
import com.example.rasyonmobil.util.Util;

import java.util.List;

public class PatientMealList extends MyBaseActivity {

    TextView countText,errorCountText,eatingCountText,notEatingText;
    DataBaseOperation dataBaseOperation;
    ListView hastaList;
    PatientMealAdapter patientMealAdapter;
    List<PatientMeal> patientMealList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_meal_list);
        initView();
        fillData();
        patientMealAdapter=new PatientMealAdapter(PatientMealList.this, patientMealList);
        hastaList.setAdapter(patientMealAdapter);

        if (MyBuild.isOnDevelopment()) {
            hastaList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                    PatientMeal selItem = patientMealAdapter.getItem(position);
                    Intent intent=new Intent(PatientMealList.this,MainActivity.class);
                    intent.putExtra("id",String.valueOf(selItem.ID));
                    startActivity(intent);
                    finish();
                }
            });
        }
    }
    public void initView(){
        hastaList   = findViewById(R.id.hastaList);
        countText   = findViewById(R.id.countText);
        errorCountText   = findViewById(R.id.errorCountText);
        eatingCountText   = findViewById(R.id.eatingCountText);
        notEatingText   = findViewById(R.id.notEatingText);

        dataBaseOperation=new DataBaseOperation(this,"");
    }
    public void fillData(){
        patientMealList=dataBaseOperation.GetPatientMealList();
        Integer count=0,hatali=0,yemekYemeyen=0,yemekYiyen=0;
        if (patientMealList.size()>0){
            for (PatientMeal patientMeal:patientMealList) {
//                if (PatientMealPostType.gonderilenHasta==patientMeal.PostedStatus){
//                    gonderilen++;
//                }else if (PatientMealPostType.yeniHasta==patientMeal.PostedStatus){
//                    yeniHasta++;
//                }else
                if (PatientMealPostType.hataliHasta==patientMeal.PostedStatus){
                    hatali++;
                }
                if (patientMeal.PatientEating==false){
                    yemekYemeyen++;
                }else{
                    yemekYiyen++;
                }
                count++;
            }
            countText.setText(count.toString());
            errorCountText.setText(hatali.toString());
            eatingCountText.setText(yemekYiyen.toString());
            notEatingText.setText(yemekYemeyen.toString());

        }else{
            informationClear();
            Toast.makeText(this,"Yemek Bilgileri Alınamadı !",Toast.LENGTH_LONG).show();
        }
    }
    public void informationClear(){
        countText.setText("---");
        errorCountText.setText("---");
        eatingCountText.setText("---");
        notEatingText.setText("---");
    }
    public void SendPatientMealData(View view){
        Util.openProgressDialog(this, "Gönderiliyor...").show();
        dataBaseOperation.PatientMealDBReflesh();
        informationClear();
//        if (patientMealList.size()>0){
//
//            for (PatientMeal patientMeal: patientMealList) {
//                if (patientMeal.PatientEating){
//                   Boolean result= dataBaseOperation.SendPatientMeal(patientMeal);
//                }
//            }
//            fillData();
//
//            Toast.makeText(this,"Hastaların Verileri Gonderildi.",Toast.LENGTH_LONG).show();
//        }
//        else{
//            Toast.makeText(this,"Gönderime Hazır Hasta Bulunamadı.",Toast.LENGTH_LONG).show();
//        }
    }
    public void setMainActivity(View view){
        Intent intent=new Intent(PatientMealList.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}
