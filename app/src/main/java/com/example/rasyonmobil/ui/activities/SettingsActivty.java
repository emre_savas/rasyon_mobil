package com.example.rasyonmobil.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rasyonmobil.R;
import com.example.rasyonmobil.backbone.MyBaseActivity;
import com.example.rasyonmobil.engines.SharedPrefencesEngine;
import com.example.rasyonmobil.util.Util;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;

public class SettingsActivty extends MyBaseActivity {

    final int REQUEST_IMAGE = 324;
    Button auth, save;
    MaterialEditText username, password, inWifiName, tahlilsonucUrl;
    ViewGroup authCont, configCont;
    ArrayList<String> mSelectPath;
    ImageView addPhoto;
    private TextView header;
    private MaterialEditText tesisAdi;
    private MaterialEditText inIP;
    private MaterialEditText outIP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        this.outIP = (MaterialEditText) findViewById(R.id.outIP);
        this.inIP = (MaterialEditText) findViewById(R.id.inIP);
        this.tesisAdi = (MaterialEditText) findViewById(R.id.tesisAdi);
        this.header = (TextView) findViewById(R.id.header);
        auth = (Button) findViewById(R.id.auth);
        save = (Button) findViewById(R.id.save);
        password = (MaterialEditText) findViewById(R.id.password);
        tahlilsonucUrl = (MaterialEditText) findViewById(R.id.tahlilsonucUrl);
        username = (MaterialEditText) findViewById(R.id.username);
        inWifiName = (MaterialEditText) findViewById(R.id.inWifiName);
        addPhoto = (ImageView) findViewById(R.id.addPhoto);
        tahlilsonucUrl.setVisibility(View.GONE);
        authCont = (ViewGroup) findViewById(R.id.authCont);
        configCont = (ViewGroup) findViewById(R.id.configCont);
        auth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeKeyBoard();
                if (username.getText().toString().matches("")) {
                    username.setError("Bu alan boş geçilemez!");
                }

                if (password.getText().toString().matches("")) {
                    password.setError("Bu alan boş geçilemez!");
                    return;
                }
                onAuthClick();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveDatasToSpref();
            }
        });
        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //openImagePickActivity();
            }
        });
    }

    private void saveDatasToSpref() {
        if (tesisAdi.getText().toString().matches("")) {
            tesisAdi.setError("Bu alan boş geçilemez!");
            return;
        }
        if (inIP.getText().toString().matches("")) {
            inIP.setError("Bu alan boş geçilemez!");
            return;
        }
        if (outIP.getText().toString().matches("")) {
            outIP.setError("Bu alan boş geçilemez!");
            return;
        }
        if (inWifiName.getText().toString().matches("")) {
            inWifiName.setError("Bu alan boş geçilemez!");
            return;
        }
       /* if (tahlilsonucUrl.getText().toString().matches("")) {
            tahlilsonucUrl.setError("Bu alan boş geçilemez!");
            return;
        }*/
//        spref().savePreferences(SharedPrefencesEngine.TAHLIL_RESULT_URL, "http://"+tahlilsonucUrl.getText().toString()+"/WinLisWeb/rapor_sonuc2.aspx?OID=");
        spref().savePreferences(SharedPrefencesEngine.IN_IP, inIP.getText().toString());
        spref().savePreferences(SharedPrefencesEngine.OUT_IP, outIP.getText().toString());
        spref().savePreferences(SharedPrefencesEngine.WIFI_NAME, inWifiName.getText().toString());
        spref().savePreferences(SharedPrefencesEngine.TESIS_ADI, tesisAdi.getText().toString());
        Toast.makeText(this, "Kaydedildi", Toast.LENGTH_SHORT).show();
        finish();
    }

//    private void openImagePickActivity() {
//        Intent intent = new Intent(SettingsActivty.this, MultiImageSelectorActivity.class);
//
//        intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, true);
//
//        // intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, maxNum);
//
//        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, MultiImageSelectorActivity.MODE_SINGLE);
//
//        if (mSelectPath != null && mSelectPath.size() > 0) {
//            intent.putExtra(MultiImageSelectorActivity.EXTRA_DEFAULT_SELECTED_LIST, mSelectPath);
//        }
//        startActivityForResult(intent, REQUEST_IMAGE);
//    }

    public SharedPrefencesEngine spref() {
        return SharedPrefencesEngine.sharedInstance(this);
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK) {
//            mSelectPath = new ArrayList<>();
//            mSelectPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
//            spref().savePreferences(SharedPrefencesEngine.TESIS_IMAGE_PATH, mSelectPath.get(0));
//            Picasso.with(this).load(new File(mSelectPath.get(0))).placeholder(R.drawable.nulldrawable).into(addPhoto);
//        } else if (resultCode == RESULT_CANCELED) {
//            Toast.makeText(getApplicationContext(), "İptal edildi.",//"Cancelled"
//                    Toast.LENGTH_SHORT).show();
//        }
//    }

    public void closeKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(username.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(password.getWindowToken(), 0);
    }

    private void onAuthClick() {
        String uName = username.getText().toString();
        String pass = password.getText().toString();
        if (uName.equals("ees") && pass.equals("aslan06")) {
//        if (uName.equals("a") && pass.equals("a")) {
            authCont.setVisibility(View.GONE);
            configCont.setVisibility(View.VISIBLE);
            inWifiName.setText(Util.getSSIDofNetwork(getApplicationContext()));
        } else {
            Toast.makeText(this, "Hatalı Kullanıcı Adı./Şifre", Toast.LENGTH_SHORT).show();
        }
        /*Util.openProgressDialog(this, "Yetki alınıyor...").show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                authCont.setVisibility(View.GONE);
                configCont.setVisibility(View.VISIBLE);
                *//*String SSID = Util.getSSIDofNetwork(getApplicationContext());
                if (SSID != null && !SSID.equals("") && !SSID.equals("<unknown ssid>")) {
                    inWifiName.setText(SSID);
                }*//*
                Util.cancelDialog();
            }
        }, 3000);*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings_activty, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
