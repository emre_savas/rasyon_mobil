package com.example.rasyonmobil.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.rasyonmobil.R;
import com.example.rasyonmobil.engines.PatientMealPostType;
import com.example.rasyonmobil.model.DietType;
import com.example.rasyonmobil.model.PatientMeal;

import java.util.List;

public class DietTypeAdapter extends ArrayAdapter<DietType> {

    private final LayoutInflater inflater;
    private final Context context;
    private ViewHolder holder;
    private final List<DietType> dietTypeList;

    public DietTypeAdapter(Context context, List<DietType> dietTypeList) {
        super(context,dietTypeList.size(), dietTypeList);
        this.context = context;
        this.dietTypeList = dietTypeList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return dietTypeList.size();
    }

    @Override
    public DietType getItem(int position) {
        return dietTypeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return dietTypeList.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.spin_item, null);

            holder = new ViewHolder();
            holder.spinnerItem =convertView.findViewById(R.id.spinnerItem);
            convertView.setTag(holder);

        }
        else{
            //Get viewholder we already created
            holder = (ViewHolder)convertView.getTag();
        }

        DietType dietType = dietTypeList.get(position);
        if(dietType != null){
            holder.spinnerItem.setText(dietType.Name+"-"+dietType.Definition);

        }
        return convertView;
    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.spin_item, null);

            holder = new ViewHolder();
            holder.spinnerItem =convertView.findViewById(R.id.spinnerItem);
            convertView.setTag(holder);

        }
        else{
            //Get viewholder we already created
            holder = (ViewHolder)convertView.getTag();
        }

        DietType dietType = dietTypeList.get(position);
        if(dietType != null){
            holder.spinnerItem.setText(dietType.Name+"-"+dietType.Definition);

        }
        return convertView;
    }

    //View Holder Pattern for better performance
    private static class ViewHolder {
        TextView spinnerItem;
    }
}
