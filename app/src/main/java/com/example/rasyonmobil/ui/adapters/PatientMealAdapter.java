package com.example.rasyonmobil.ui.adapters;

import android.content.Context;
import android.graphics.Movie;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.example.rasyonmobil.R;
import com.example.rasyonmobil.engines.PatientMealPostType;
import com.example.rasyonmobil.model.PatientMeal;

import java.util.List;

public class PatientMealAdapter extends ArrayAdapter<PatientMeal> {

    private final LayoutInflater inflater;
    private final Context context;
    private ViewHolder holder;
    private final List<PatientMeal> patientMeals;

    public PatientMealAdapter(Context context, List<PatientMeal> patientMeals) {
        super(context,0, patientMeals);
        this.context = context;
        this.patientMeals = patientMeals;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return patientMeals.size();
    }

    @Override
    public PatientMeal getItem(int position) {
        return patientMeals.get(position);
    }

    @Override
    public long getItemId(int position) {
        return patientMeals.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.patientmeal_list_view_item, null);

            holder = new ViewHolder();
            holder.patientNameText = (TextView) convertView.findViewById(R.id.patientNameText);
            holder.patientPostStatusText = (TextView) convertView.findViewById(R.id.patientPostStatusText);
            holder.errorMessageText = (TextView) convertView.findViewById(R.id.errorMessageText);
            convertView.setTag(holder);

        }
        else{
            //Get viewholder we already created
            holder = (ViewHolder)convertView.getTag();
        }

        PatientMeal patientMeal = patientMeals.get(position);
        if(patientMeal != null){
            String temp=patientMeal.PatientEating?"Yemek Verildi":"Yemek Verilmedi";
            String compainer=patientMeal.CompanierHasMeal?"Refakatçi Yemek Var":"Refakatçi Yemek Yok";
            holder.patientPostStatusText.setText(patientMeal.MealTypeName+" / "+statusText(patientMeal.PostedStatus)+" / "+temp+" / "+compainer);
            holder.patientNameText.setText(patientMeal.PatientFullName);
            if (patientMeal.ErrorMessage.contains("")==false){
                holder.errorMessageText.setText(patientMeal.ErrorMessage);
            }else{
                holder.errorMessageText.setVisibility(View.GONE);
            }

        }
        return convertView;
    }

    //View Holder Pattern for better performance
    private static class ViewHolder {
        TextView patientPostStatusText;
        TextView patientNameText;
        TextView errorMessageText;
    }
    public String statusText(Integer status){
        String result="";
        if (PatientMealPostType.gonderilenHasta==status){
            result = "Gönderilen Hasta";
        }else if (PatientMealPostType.yeniHasta==status){
            result = "Yeni Hasta";
        }else if (PatientMealPostType.hataliHasta==status){
            result = "Hatalı Gönderilen Hasta";
        }
        return result;
    }
}
