package com.example.rasyonmobil.util;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.BoringLayout;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.rasyonmobil.BuildConfig;
import com.example.rasyonmobil.backbone.MyApplication;
import com.example.rasyonmobil.backbone.MyBaseActivity;
import com.example.rasyonmobil.engines.PatientMealPostType;
import com.example.rasyonmobil.engines.SharedPrefencesEngine;
import com.example.rasyonmobil.model.DietType;
import com.example.rasyonmobil.model.PatientMeal;
import com.example.rasyonmobil.model.User;
import com.example.rasyonmobil.model.database.DietTypeDatabaseHandler;
import com.example.rasyonmobil.model.database.PatientMealDatabaseHandler;
import com.example.rasyonmobil.model.database.UserDatabaseHandler;
import com.example.rasyonmobil.network.IRasyonService;
import com.example.rasyonmobil.network.RasyonServiceClient;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataBaseOperation {

    UserDatabaseHandler userDatabaseHandler;
    PatientMealDatabaseHandler patientMealDatabaseHandler;
    DietTypeDatabaseHandler dietTypeDatabaseHandler;
    IRasyonService rasyonService;
    Context context;
    Integer araOgunStatus=0;
    String HASTANE_ADI="";

    public DataBaseOperation(Context context,String HASTANE_ADI){
        userDatabaseHandler=new UserDatabaseHandler(context);
        dietTypeDatabaseHandler=new DietTypeDatabaseHandler(context);
        patientMealDatabaseHandler=new PatientMealDatabaseHandler(context);
        rasyonService= RasyonServiceClient.getClient().create(IRasyonService.class);
        this.context=context;
        this.HASTANE_ADI=HASTANE_ADI;
    }
    public void TestService(){
        final Call<String> userListResponse = rasyonService.healthcheck();
        userListResponse.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.raw().code() == 200){
                    if(response.body() != null && response.isSuccessful()){
                        Toast.makeText(context, "Test Başarılı" +response.body()+ ", message:"+response.message(), Toast.LENGTH_SHORT).show();
                    }else{
                        Util.cancelDialog();
                        Toast.makeText(context, "Test Başarısız !. " +response.body()+ " message:"+response.message(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Util.cancelDialog();
                    Toast.makeText(context, "Test Hata Kodu2:Servis bağlantısı başarısız."+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Util.cancelDialog();
                Toast.makeText(context, "Test Servis bağlantısı yapılamıyor."+t.getMessage()+ MyApplication.getMealDeliveryUri(), Toast.LENGTH_SHORT).show();

            }
        });


    }
    public void UserDBReflesh(){
        final Call<List<User>> userListResponse = rasyonService.GetUserList();
        userListResponse.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if(response.raw().code() == 200){
                    if(response.body() != null && response.isSuccessful()){
                        userDatabaseHandler.onUpgrade();
                        List<User> userList = response.body();
                        if(userList.size() > 0){
                            for (User user :
                                    userList) {
                                userDatabaseHandler.addUser(user);
                            }
                            DietTypeDBReflesh();
                            Toast.makeText(context, "Kullanıcı Listesi Güncellendi.", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(context, "Kullanıcı listesi alınamadı.", Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        Util.cancelDialog();
                        Toast.makeText(context, "Hata Kodu1:Kullanıcı yükleme , işlem gerçekleşemedi."+response.message(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Util.cancelDialog();
                    Toast.makeText(context, "Hata Kodu2:Kullanıcı yükleme,Servis bağlantısı başarısız."+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Util.cancelDialog();
                Toast.makeText(context, "Servis bağlantısı yapılamıyor."+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }
    public void DietTypeDBReflesh(){
        final Call<List<DietType>> getDietTypeList = rasyonService.GetDietTypeList();
        getDietTypeList.enqueue(new Callback<List<DietType>>() {
            @Override
            public void onResponse(Call<List<DietType>> call, Response<List<DietType>> response) {
                if(response.raw().code() == 200){
                    if(response.body() != null && response.isSuccessful()){
                        dietTypeDatabaseHandler.onUpgrade();
                        List<DietType> dietTypes = response.body();
                        if(dietTypes.size() > 0){
                            for (DietType dietType :
                                    dietTypes) {
                                dietTypeDatabaseHandler.addDietType(dietType);
                            }
                            PatientMealDBReflesh();
                            Toast.makeText(context, "Diet tür Listesi Güncellendi.", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(context, "Diet tür listesi alınamadı.", Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        Util.cancelDialog();
                        Toast.makeText(context, "Hata Kodu1:Hasta yükleme , işlem gerçekleşemedi."+response.message(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Util.cancelDialog();
                    Toast.makeText(context, "Hata Kodu2:Hasta yükleme,Servis bağlantısı başarısız."+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<DietType>> call, Throwable t) {
                Util.cancelDialog();
                Toast.makeText(context, "Servis bağlantısı yapılamıyor."+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });



    }
    public void PatientMealDBReflesh(){
        List<PatientMeal> patientMealList=patientMealDatabaseHandler.getPatientMealListByEatingAndStatus();
        patientMealDatabaseHandler.onUpgrade();
        if (patientMealList.size()>0){
            for (PatientMeal patientMeal :
                    patientMealList) {
                patientMeal.ID=0;
                patientMealDatabaseHandler.addPatientMeal(patientMeal);
                Boolean result= SendPatientMeal(patientMeal);
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String currentDate = sdf.format(new Date());
        Log.i("refleshDate",currentDate);

        final Call<List<PatientMeal>> PatientListCall = rasyonService.GetMealList(currentDate);
        PatientListCall.enqueue(new Callback<List<PatientMeal>>() {
            @Override
            public void onResponse(Call<List<PatientMeal>> call, Response<List<PatientMeal>> response) {
                if(response.raw().code() == 200){
                    if(response.body() != null && response.isSuccessful()){
//                        patientMealDatabaseHandler.onUpgrade();

                        List<PatientMeal> patientMeals = response.body();
                        if(patientMeals.size() > 0){
                            for (PatientMeal patientMeal :
                                    patientMeals) {

                                    patientMeal.NewDietTypeID=0;
                                    patientMeal.PostedStatus=PatientMealPostType.yeniHasta;
                                    patientMealDatabaseHandler.addPatientMeal(patientMeal);
                            }
                            Util.cancelDialog();
                            Toast.makeText(context, "Hasta Listesi Güncellendi.", Toast.LENGTH_SHORT).show();
                        }else{
                            Util.cancelDialog();
                            Toast.makeText(context, "Hasta listesi alınamadı.", Toast.LENGTH_SHORT).show();
                        }
                        Util.cancelDialog();
                    }else{
                        Toast.makeText(context, "Hata Kodu1:Hasta yükleme , işlem gerçekleşemedi."+response.message(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(context, "Hata Kodu2:Hasta yükleme,Servis bağlantısı başarısız."+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<PatientMeal>> call, Throwable t) {
                Toast.makeText(context, "Servis bağlantısı yapılamıyor."+t.getMessage(), Toast.LENGTH_SHORT).show();
                Util.cancelDialog();
            }
        });
    }
    public void setPatientMealDatabaseHandlerUpgrade(){
        patientMealDatabaseHandler.onUpgrade();
    }
    public User GetUserByUserName(String username){
        User dbUser=userDatabaseHandler.getUser(username);
        List<User> dbUsers=userDatabaseHandler.getAllUsers();
        return dbUser;
    }
    public List<PatientMeal> GetPatientMealList(){
        List<PatientMeal> patientMeals=patientMealDatabaseHandler.getAllPatientMeals();
        return patientMeals;
    }
    public List<DietType> GetDietTypeList(){
        FindMeal(false);
        List<DietType> dietTypes=dietTypeDatabaseHandler.getAllDietType(araOgunStatus);
        return dietTypes;
    }
    public Integer GetPatientMealCountByStatus(Integer status){
        Integer count=patientMealDatabaseHandler.getDataCount(status);
        return count;
    }
    public Boolean SendPatientMeal(final PatientMeal patientMeal){
        try {
            patientMeal.PostAppVers= BuildConfig.VERSION_NAME;
            final Call<String> sendDataCall = rasyonService.DeliverMeal(patientMeal);
            sendDataCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if(response.raw().code() == 200){
                        if(response.body() != null && response.isSuccessful()){
                            String result = response.body();
                            if(result !=null && result.contains("1")){
                                patientMeal.PostedStatus= PatientMealPostType.gonderilenHasta;
                            }else{
                                patientMeal.ErrorMessage=result;
                                patientMeal.PostedStatus= PatientMealPostType.hataliHasta;
                            }
                        }else{
                            patientMeal.PostedStatus= PatientMealPostType.hataliHasta;
                        }
                    }else{
                        patientMeal.PostedStatus= PatientMealPostType.hataliHasta;
                    }
                    patientMealDatabaseHandler.updatePatientMeal(patientMeal);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    patientMeal.PostedStatus= PatientMealPostType.hataliHasta;
                    patientMealDatabaseHandler.updatePatientMeal(patientMeal);
//                    Util.cancelDialog();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            Log.i("PostHata",e.getMessage());
            patientMeal.PostedStatus= PatientMealPostType.hataliHasta;
            patientMealDatabaseHandler.updatePatientMeal(patientMeal);
        }
        return true;
    }
    public PatientMeal epatientMealDatabaseHandler(String receptionNo){
        PatientMeal patientMeal=patientMealDatabaseHandler.getPatientMealByReceptionNo(receptionNo,FindMeal(false));
        return patientMeal;
    }
    public void test(){

        List<PatientMeal> list=GetPatientMealList();
        for (PatientMeal patientMeal :
                list) {
            String[] temp=patientMeal.ReceptionNo.split("-");
            if (temp[0].matches("15824428")){
                Log.i("receptionno","buldu");
                Log.i("receptionno",patientMeal.MealTypeName);
            }else if (patientMeal.ReceptionNo.matches("15759983-4")){
                Log.i("receptionno","buldu-4");
            }
        }
    }
    public PatientMeal getPatientMealByReceptionNoNotDash(String receptionNo,Boolean araOgun){
        String ogun="";
        if (araOgun){
            ogun=FindMeal(true);
            return patientMealDatabaseHandler.getPatientMealByReceptionNo(receptionNo,ogun);
        }else{
            ogun=FindMeal(false);
            return patientMealDatabaseHandler.getPatientMealByReceptionNoNotDash(receptionNo,ogun);
        }
    }
    public Integer AddPatientMeal(PatientMeal patientMeal){
       return patientMealDatabaseHandler.addPatientMeal(patientMeal);
    }

    public String FindMeal(Boolean araOgun){

        if (HASTANE_ADI=="GOP"){
            if (araOgun)
                return MealTypeSnackGOP();
            else
                return MealTypeGOP();
        }else if(HASTANE_ADI=="Taksim"){
//            if (araOgun)
//                return araOgunBul();
//            else
//                return ogunBul();
            if (araOgun)
                return MealTypeSnackGOP();
            else
                return MealTypeGOP();
        } else{
            return "Hastane seçimi yapılamadı.";
        }
    }

    public String MealTypeGOP(){
        String result="";
        int hour24hrs = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int minute = Calendar.getInstance().get(Calendar.MINUTE);

        if ((hour24hrs>5 && hour24hrs<9) || (hour24hrs==9 && minute<30) || (hour24hrs==5 && minute>=30)){
            result="Sabah";
            araOgunStatus=1;
        } else  if ((hour24hrs>9 && hour24hrs<11) || (hour24hrs==9 && minute>=30)){
            result="Sabah Ara Öğün";
        }else if ((hour24hrs>11 && hour24hrs<14) || (hour24hrs==11 && minute>=30)){
            result="Öğle";
            araOgunStatus=1;
        }else if ((hour24hrs>=14 && hour24hrs<16) ){
            result="Öğle Ara Öğün";
        }else if ((hour24hrs>16 && hour24hrs<=19) || (hour24hrs==16 && minute>=30)){
            result="Akşam";
            araOgunStatus=2;
        }else if ((hour24hrs>16 && hour24hrs<=19) || (hour24hrs==16 && minute>=30)){
            result="Akşam Ara Öğün";
            araOgunStatus=2;
        }
        else{
            return "ogun disi";
        }
        return result;
    }
    public String MealTypeSnackGOP(){
        String result="";
        int hour24hrs = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int minute = Calendar.getInstance().get(Calendar.MINUTE);

        if ((hour24hrs>9 && hour24hrs<11) || (hour24hrs==9 && minute>=30)){
            result="Sabah Ara Öğün";
        }else if ((hour24hrs>=14 && hour24hrs<16) ){
            result="Öğle Ara Öğün";
        }else if ((hour24hrs>16 && hour24hrs<=19) || (hour24hrs==16 && minute>=30)){
            result="Akşam Ara Öğün";
            araOgunStatus=2;
        }
        else{
            return "ogun disi";
        }
        return result;
    }

    public String MealTypeTaksim(){
        String result="";
        int hour24hrs = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int minute = Calendar.getInstance().get(Calendar.MINUTE);

        if ((hour24hrs>=5 && hour24hrs<11) || (hour24hrs==11 && minute<30) || (hour24hrs==5 && minute>=30)){
            result="Sabah";
            araOgunStatus=1;
        }else if ((hour24hrs>11 && hour24hrs<=15) || (hour24hrs==11 && minute>=30)){
            result="Öğle";
            araOgunStatus=1;
        }else if ((hour24hrs>16 && hour24hrs<=19) || (hour24hrs==16 && minute>=30)){
            result="Akşam";
            araOgunStatus=2;
        }
        else{
           return "ogun disi";
        }
        return result;
    }
    public String MealTypeSnackTaksim(){
        String result="";
        int hour24hrs = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int minute = Calendar.getInstance().get(Calendar.MINUTE);

        if ((hour24hrs>=5 && hour24hrs<11) || (hour24hrs==11 && minute<30) || (hour24hrs==5 && minute>=30)){
            result="Sabah Ara Öğün";
        }else if ((hour24hrs>11 && hour24hrs<=15) || (hour24hrs==11 && minute>=30)){
            result="Öğle Ara Öğün";
        }else if ((hour24hrs>16 && hour24hrs<=19) || (hour24hrs==16 && minute>=30)){
            result="Akşam Ara Öğün";
        }
        else{
           return "ogun disi";
        }
        return result;
    }
}
